package com.example.samsung.tcc.utilities;

import android.content.Context;
import android.util.Base64;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.Response.ErrorListener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rebeca de Melo on 01/05/2017.
 */
public class CustomJsonArrayRequest extends JsonRequest<JSONArray> {

    private String baUsuario;
    private String baSenha;

    public CustomJsonArrayRequest(int method, String url, JSONObject jsonRequest, Context context,
                                  Listener<JSONArray> listener, ErrorListener errorListener) {
        super(method, url, /*(jsonRequest == null) ? null : jsonRequest.toString()*/jsonRequest.toString(),
                listener, errorListener);
        try {
            baUsuario = PropertySource.getProperty("resysclagem.service.ba.usuario", context);
            baSenha = PropertySource.getProperty("resysclagem.service.ba.senha", context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> params = new HashMap<String, String>();
        String creds = String.format("%s:%s",baUsuario,baSenha);
        String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
        params.put("Authorization", auth);
        return params;
    }

    @Override
    protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
            return Response.success(new JSONArray (jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }
}
