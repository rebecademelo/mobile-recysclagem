package com.example.samsung.tcc.classes.subcls;

import java.util.List;

public class HoraAtend {

	private List<DiaSemana> diasSemana;

	public HoraAtend(List<DiaSemana> diasSemana) {
		super();
		this.diasSemana = diasSemana;
	}

	public List<DiaSemana> getDiasSemana() {
		return diasSemana;
	}

	public void setDiasSemana(List<DiaSemana> diasSemana) {
		this.diasSemana = diasSemana;
	}

	public int compare(DiaSemana o1, DiaSemana o2) {
		return o1.getDia() - o2.getDia();
	}
}

