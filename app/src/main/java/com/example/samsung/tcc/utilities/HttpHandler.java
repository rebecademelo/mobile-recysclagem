package com.example.samsung.tcc.utilities;

import android.content.Context;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rebeca de Melo on 22/03/2017.
 */
public class HttpHandler {

    private String baUsuario;
    private String baSenha;

    // faz a conexão, o request e a captura da resposta
    public String makeServiceCall(String reqURL, String method, String params, Context context){
        String response = null;
        BufferedReader br = null;
        try {
            // basic auth
            baUsuario = PropertySource.getProperty("resysclagem.service.ba.usuario", context);
            baSenha = PropertySource.getProperty("resysclagem.service.ba.senha", context);
            String creds = String.format("%s:%s",baUsuario,baSenha);
            String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);

            // faz o request
            URL url = new URL(reqURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // add header
            conn.setRequestProperty("Authorization", auth);
            if(method.equals("GET")){
                conn.setRequestMethod("GET");
                // pega a resposta
                InputStream in = new BufferedInputStream(conn.getInputStream());
                response = convertStreamToString(in);
            }else if(method.equals("POST")){
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                OutputStreamWriter output = new OutputStreamWriter(conn.getOutputStream());
                //OutputStream output = conn.getOutputStream();
                //BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));
                output.write(params);
                output.flush();
                //output.close();
                br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = br.readLine()) != null){
                    sb.append(line).append('\n');
                }
                response = sb.toString();
                /*
                InputStream in = new BufferedInputStream(conn.getInputStream());
                response = convertStreamToString(in);
                int responseCode = conn.getResponseCode();

                if(responseCode == HttpURLConnection.HTTP_OK){
                    InputStream in = new BufferedInputStream(conn.getInputStream());
                    response = convertStreamToString(in);
                }else{
                    response = String.valueOf(HttpURLConnection.HTTP_UNAVAILABLE);
                }*/
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return response;
    }
    // lê a InputStream e monta a string de resposta que contem os dados
    private String convertStreamToString(InputStream is){
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null){
                sb.append(line).append('\n');
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                is.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
    //
    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = null;
        result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if(first){
                first = false;
            }else {
                result.append("&");
            }
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        return result.toString();
    }
}
