package com.example.samsung.tcc.usuario;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.samsung.tcc.R;
import com.example.samsung.tcc.classes.MsgResponse;
import com.example.samsung.tcc.classes.SessionManager;
import com.example.samsung.tcc.classes.Usuario;
import com.example.samsung.tcc.classes.VolleyCallback;
import com.example.samsung.tcc.utilities.CheckHost;
import com.example.samsung.tcc.utilities.CustomJSONObjectRequest;
import com.example.samsung.tcc.utilities.CustomJsonArrayRequest;
import com.example.samsung.tcc.utilities.PathClass;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class ContaFragment extends Fragment {

    private EditText nomeUsuario;
    private EditText datanascUsuario;
    private EditText emailUsuario;
    private EditText senhaUsuario;
    private Button logout;
    private Button atualizar;
    private SessionManager session;
    private Gson gson = new Gson();
    private ProgressDialog pDialog;
    private com.example.samsung.tcc.classes.Request request;
    private PathClass path;
    private String url = "";
    private JSONObject requestObj;
    private CheckHost host = new CheckHost();
    private Usuario usuario;
    private int cod;
    private String nome;
    private String datanasc;
    private String email;
    private String token;
    private Usuario usuarioAlterado;
    private String nomeAlterado;
    private String datanascAlterado;
    private String emailAlterado;
    private String senhaAlterada;
    private ArrayList<Usuario> usuarios = new ArrayList<>();

    public ContaFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_conta, container, false);

        session = new SessionManager(getContext());

        nomeUsuario = (EditText) view.findViewById(R.id.nome);
        datanascUsuario = (EditText) view.findViewById(R.id.datanasc);
        emailUsuario = (EditText) view.findViewById(R.id.email);
        senhaUsuario = (EditText) view.findViewById(R.id.senha);
        logout = (Button) view.findViewById(R.id.logout);
        atualizar = (Button) view.findViewById(R.id.btnAlterar);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(LoginManager.getInstance() != null){
                    LoginManager.getInstance().logOut();
                }
                session.logoutUsuario();
            }
        });

        atualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDialog = new ProgressDialog(getContext());
                pDialog.setCancelable(false);
                pDialog.show();

                Thread usuarioThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        alteraInfoUsuario(new VolleyCallback() {
                            @Override
                            public void onSuccess(JSONArray response) {

                            }

                            @Override
                            public void onSuccess(JSONObject response) {
                                hideProgressDialog();
                                MsgResponse msgResponse = gson.fromJson(response.toString(), MsgResponse.class);
                                if (msgResponse.isStatus()){
                                    Log.i("updateUsuario", msgResponse.getMessage());
                                    session.logoutUsuario();
                                } else {
                                    Log.i("updateUsuarioError", msgResponse.getMessage());
                                    Toast.makeText(getContext(),
                                            "Ocorreu um problema na hora de atualizar suas informações. Por favor tente mais tarde",
                                            Toast.LENGTH_LONG)
                                            .show();
                                }
                            }

                            @Override
                            public void onError(VolleyError error) {
                                final VolleyError volleyError = error;
                                try {
                                    if (error.networkResponse.statusCode != 0) {
                                        hideProgressDialog();
                                        Log.i("statusCode", String.valueOf(error.networkResponse.statusCode));
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity(),
                                                        "Ocorreu um problema na hora de listar os resíduos favoritos: " + volleyError.networkResponse.statusCode,
                                                        Toast.LENGTH_LONG)
                                                        .show();
                                            }
                                        });
                                    } else if (error instanceof NetworkError) {
                                        hideProgressDialog();
                                        Log.i("networkError", error.getMessage());
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity(),
                                                        "Network Error: " + volleyError.getMessage(),
                                                        Toast.LENGTH_LONG)
                                                        .show();
                                            }

                                        });
                                    } else if (error instanceof ServerError) {
                                        hideProgressDialog();
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity(),
                                                        "Server Error: " + volleyError.getMessage(),
                                                        Toast.LENGTH_LONG)
                                                        .show();
                                            }

                                        });
                                    } else if (error instanceof AuthFailureError) {
                                        Log.i("authError", error.getMessage());
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity(),
                                                        "AuthFailure Error: " + volleyError.getMessage(),
                                                        Toast.LENGTH_LONG)
                                                        .show();
                                            }

                                        });
                                    } else if (error instanceof ParseError) {
                                        hideProgressDialog();
                                        Log.i("parseError", error.getMessage());
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity(),
                                                        "Parse Error: " + volleyError.getMessage(),
                                                        Toast.LENGTH_LONG)
                                                        .show();
                                            }

                                        });
                                    } else if (error instanceof NoConnectionError) {
                                        hideProgressDialog();
                                        Log.i("noconnectionError", error.getMessage());
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity(),
                                                        "NoConnection Error: " + volleyError.getMessage(),
                                                        Toast.LENGTH_LONG)
                                                        .show();
                                            }

                                        });
                                    } else if (error instanceof TimeoutError) {
                                        hideProgressDialog();
                                        Log.i("timeoutError", error.getMessage());
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity(),
                                                        "Timeout Error: " + volleyError.getMessage(),
                                                        Toast.LENGTH_LONG)
                                                        .show();
                                            }

                                        });
                                    } else {
                                        Log.i("else", error.getMessage());
                                        hideProgressDialog();
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity(),
                                                        "Ocorreu um problema na hora de listar os resíduos favoritos: " + volleyError.getMessage(),
                                                        Toast.LENGTH_LONG)
                                                        .show();
                                            }

                                        });
                                        /*Toast.makeText(getContext(),
                                                error.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();*/
                                    }
                                } catch (final Exception e) {
                                    Log.i("exececaoListaFav", error.getMessage() + " & " + e.getMessage());
                                    hideProgressDialog();
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),
                                                    "Ocorreu uma exceção na hora de listar os resíduos favoritos: " + e.getMessage(),
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                    });
                                }
                            }
                        });
                    }
                });

                usuarioThread.start();
            }
        });

        session.checkLogin();
        HashMap<String, String> usuarioLogado = session.getInfoUsuario();
        cod = Integer.parseInt(usuarioLogado.get(SessionManager.KEY_CODU));
        nome = usuarioLogado.get(SessionManager.KEY_NOME);
        datanasc = usuarioLogado.get(SessionManager.KEY_DATA);
        email = usuarioLogado.get(SessionManager.KEY_EMAIL);
        token = usuarioLogado.get(SessionManager.KEY_TOKEN);

        nomeUsuario.setText(nome);
        datanascUsuario.setText(datanasc);
        emailUsuario.setText(email);

        return view;
    }

    public void alteraInfoUsuario(final VolleyCallback callback){
        path = new PathClass(getContext());
        try {
            url = path.getServerPath() + "/usuario";
            usuario = new Usuario(cod, nome, datanasc, email);

            nomeAlterado = nomeUsuario.getText().toString();
            datanascAlterado = datanascUsuario.getText().toString();
            emailAlterado = emailUsuario.getText().toString();
            senhaAlterada = senhaUsuario.getText().toString();

            usuarioAlterado = new Usuario(cod, nomeAlterado, datanascAlterado, emailAlterado, senhaAlterada);

            usuarios.add(usuario);
            usuarios.add(usuarioAlterado);

            request = new com.example.samsung.tcc.classes.Request(gson.toJson(usuarios), email, token);
            requestObj = new JSONObject(gson.toJson(request));

            CustomJSONObjectRequest alteraUsuarioReq = new CustomJSONObjectRequest(Request.Method.PUT , url, requestObj, getContext(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            callback.onSuccess(response);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            callback.onError(error);
                        }
                    });
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(alteraUsuarioReq);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void hideProgressDialog(){
        if(pDialog.isShowing()){
            pDialog.dismiss();
        }
    }
}
