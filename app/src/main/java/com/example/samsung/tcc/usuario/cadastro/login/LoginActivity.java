package com.example.samsung.tcc.usuario.cadastro.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.samsung.tcc.R;
import com.example.samsung.tcc.classes.ContaUsuario;
import com.example.samsung.tcc.classes.Request;
import com.example.samsung.tcc.classes.SessionManager;
import com.example.samsung.tcc.classes.Usuario;
import com.example.samsung.tcc.drawer.DrawerActivity;
import com.example.samsung.tcc.utilities.CustomJSONObjectRequest;
import com.example.samsung.tcc.utilities.PathClass;
import com.example.samsung.tcc.utilities.SHAHelper;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class LoginActivity extends AppCompatActivity {
    public static final String REQUEST_TAG = "AutenticacaoUsuario";
    private EditText email;
    private EditText senha;
    private Button login;
    private TextView naoTemLogin;
    private Gson gson = new Gson();
    private ProgressDialog pDialog;
    private Usuario usuario;
    private SessionManager session;
    private PathClass path;
    private String url = "";
    private JSONObject jsonObject;

    private TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        path = new PathClass(LoginActivity.this);

        session = new SessionManager(getApplicationContext());

        email = (EditText) findViewById(R.id.emailET);
        senha = (EditText) findViewById(R.id.senhaET);
        login = (Button) findViewById(R.id.btLogin);
        naoTemLogin = (TextView) findViewById(R.id.textAindaNSouCadastrado);

        tv = (TextView) findViewById(R.id.textView);

        naoTemLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, TipoCadastroActivity.class);
                startActivity(intent);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginUsuario();
            }
        });
    }

    private void loginUsuario(){
        String emailLogin = email.getText().toString();
        String senhaL = senha.getText().toString();
        SHAHelper shaHelper = new SHAHelper();
        String senhaLogin = shaHelper.getSHA256( senha.getText().toString() );
        if(!validaCampos(emailLogin, senhaL)){

            usuario = new Usuario(emailLogin, senhaLogin);

            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Logando...");
            pDialog.show();

            path = new PathClass(this);
            try {
                url = path.getServerPath() + "/usuario/session";
                jsonObject = new JSONObject(gson.toJson(usuario));
                /*request = new com.example.samsung.tcc.classes.Request(gson.toJson(usuario), emailU, token);
                requestObj = new JSONObject(gson.toJson(request));*/

                CustomJSONObjectRequest stringRequest = new CustomJSONObjectRequest(url, jsonObject, getBaseContext(),
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                ContaUsuario contaUsuario = (ContaUsuario) gson.fromJson(response.toString(), ContaUsuario.class);
                                if(contaUsuario.getLoginExists() && contaUsuario.getPasswordConfirmed()) {
                                    Request usuarioRequest = contaUsuario.getRequest();
                                    Usuario usuarioLogado = gson.fromJson(usuarioRequest.getObjeto(), Usuario.class);
                                    int cod = usuarioLogado.getCodU();
                                    String nome = usuarioLogado.getNome();
                                    String datanasc = usuarioLogado.getDatanasc();
                                    String email = usuarioLogado.getEmail();
                                    session.loginSession(cod, nome, datanasc, email, usuarioRequest.getToken());
                                    pDialog.hide();
                                    Intent intent = new Intent(LoginActivity.this, DrawerActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    pDialog.hide();
                                    if(contaUsuario.getLoginExists()){
                                        Toast.makeText(getBaseContext(), "Senha incorreta!", Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(getBaseContext(), "Este usuário não existe!", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                try {
                                    if (error.networkResponse.statusCode != 0) {
                                        tv.setText(getString(R.string.mensagem_erro_response) + error.networkResponse.statusCode);
                                        pDialog.hide();
                                    } else {
                                        tv.setText(getString(R.string.mensagem_erro_get) + error.getMessage());
                                        pDialog.hide();
                                    }
                                } catch(Exception e){
                                    tv.setText(getString(R.string.exception_mensagem) + e.getMessage());
                                    pDialog.hide();
                                }
                            }
                        });
                RequestQueue requestQueue = Volley.newRequestQueue(this);
                requestQueue.add(stringRequest);
            } catch (JSONException e) {
                e.printStackTrace();
                tv.setText(R.string.json_erro_criar + " " + e);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean validaCampos(String emailU, String senhaU) {
        if (TextUtils.isEmpty(emailU)) {
            email.requestFocus();
            email.setError("Por favor digite seu email!");
            return true;
        } else if (TextUtils.isEmpty(senhaU)) {
            senha.requestFocus();
            senha.setError("Por favor digite sua senha!");
            return true;
        }
        return false;
    }
}
