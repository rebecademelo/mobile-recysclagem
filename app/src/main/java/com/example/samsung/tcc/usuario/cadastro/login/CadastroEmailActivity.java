package com.example.samsung.tcc.usuario.cadastro.login;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.samsung.tcc.R;
import com.example.samsung.tcc.classes.ContaUsuario;
import com.example.samsung.tcc.classes.MsgResponse;
import com.example.samsung.tcc.classes.Request;
import com.example.samsung.tcc.classes.SessionManager;
import com.example.samsung.tcc.classes.Usuario;
import com.example.samsung.tcc.drawer.DrawerActivity;
import com.example.samsung.tcc.utilities.CustomJSONObjectRequest;
import com.example.samsung.tcc.utilities.PathClass;
import com.example.samsung.tcc.utilities.SHAHelper;
import com.google.gson.Gson;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class CadastroEmailActivity extends AppCompatActivity{
    private Usuario usuario;
    private EditText nome;
    private EditText datanasc;
    private EditText email;
    private EditText senha;
    private Button cadastrar;
    private RequestQueue mQueue;
    private Gson gson = new Gson();
    private PathClass path;
    private String url = "";
    private SessionManager session;
    private Calendar calendar;

    private TextView tv2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_email);

        calendar = Calendar.getInstance();

        nome = (EditText) findViewById(R.id.nomeET);
        datanasc = (EditText) findViewById(R.id.datanascET);
        email = (EditText) findViewById(R.id.emailET);
        senha = (EditText) findViewById(R.id.senhaET);
        cadastrar = (Button) findViewById(R.id.btCadastrar);

        tv2 = (TextView) findViewById(R.id.tv2);

        cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cadastroUsuario();
            }
        });

        /*datanasc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(CadastroEmailActivity.this, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });*/
        datanasc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(CadastroEmailActivity.this, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }
    };

    private void updateLabel() {
        String dateFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

        datanasc.setText(sdf.format(calendar.getTime()));
    }

    private void cadastroUsuario(){
        String nomeCadastro = nome.getText().toString();
        String datanascCadastro = datanasc.getText().toString();
        String emailCadastro = email.getText().toString();
        SHAHelper shaHelper = new SHAHelper();
        String senhaCadastro = shaHelper.getSHA256( senha.getText().toString() );
        session = new SessionManager(this);

        usuario = new Usuario(nomeCadastro, datanascCadastro, emailCadastro, senhaCadastro);
        path = new PathClass(this);

        try {
            url = path.getServerPath() + "/usuario/cadastro";
            /*request = new com.example.samsung.tcc.classes.Request(gson.toJson(usuario), emailU, token);
            requestObj = new JSONObject(gson.toJson(request));*/

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("nome", nomeCadastro);
            jsonObject.put("datanasc", datanascCadastro);
            jsonObject.put("email", emailCadastro);
            jsonObject.put("senha", senhaCadastro);

            CustomJSONObjectRequest stringRequest = new CustomJSONObjectRequest(url, jsonObject, getBaseContext(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            MsgResponse msgRes = gson.fromJson(response.toString(), MsgResponse.class);
                            ContaUsuario contaUsuario = (ContaUsuario) gson.fromJson(msgRes.getExtra(), ContaUsuario.class);
                            if(msgRes.isStatus()){
                                Request usuarioRequest = contaUsuario.getRequest();
                                usuario = gson.fromJson(usuarioRequest.getObjeto(), Usuario.class);

                                int cod = usuario.getCodU();
                                String nome = usuario.getNome();
                                String datanasc = usuario.getDatanasc();
                                String email = usuario.getEmail();
                                String token = usuarioRequest.getToken();
                                session.loginSession(cod, nome, datanasc, email, token);

                                Intent intent = new Intent(CadastroEmailActivity.this, DrawerActivity.class);
                                startActivity(intent);
                                finish();
                            } else { // se o usuario ja existir
                                Toast.makeText(CadastroEmailActivity.this, msgRes.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                if (error.networkResponse.statusCode != 0) {
                                    tv2.setText(getString(R.string.mensagem_erro_response) + error.networkResponse.statusCode);
                                } else {
                                    tv2.setText(getString(R.string.mensagem_erro_get) + error.getMessage());
                                }
                            } catch(Exception e){
                                tv2.setText(getString(R.string.exception_mensagem) + e.getMessage());
                            }
                        }
                    });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
            tv2.setText(R.string.json_erro_criar + " " + e);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
