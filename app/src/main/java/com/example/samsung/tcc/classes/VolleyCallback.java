package com.example.samsung.tcc.classes;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Rebeca de Melo on 11/06/2017.
 */
public interface VolleyCallback {
    void onSuccess(JSONArray response);
    void onSuccess(JSONObject response);
    void onError(VolleyError error);
}
