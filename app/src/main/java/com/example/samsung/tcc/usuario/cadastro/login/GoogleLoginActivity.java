package com.example.samsung.tcc.usuario.cadastro.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.samsung.tcc.R;
import com.example.samsung.tcc.classes.SessionManager;
import com.example.samsung.tcc.drawer.DrawerActivity;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;

public class GoogleLoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient mGoogleApiClient;
    private int RC_SIGN_IN = 0;
    private EditText datanascG;
    private Button loginG;
    private ProgressDialog mProgressDialog;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_google_login);

        session = new SessionManager(getApplicationContext());

        datanascG = (EditText) findViewById(R.id.datanascG);
        loginG = (Button) findViewById(R.id.dataNLogin);

        // obtem as informações do usuario
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // cria um GoogleApiClient com acesso a API Google Sign-In
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // se as credencias do usuario sao validas, o OptionalPendingResult será "done" e o GoogleSignInResult ficara disponivel instantaneamente
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
        // se o usuario nao estava logado previamente no aparelho ou o login foi expirado, o sera tentado fazer o login assincronamente
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            Intent i = new Intent(GoogleLoginActivity.this, DrawerActivity.class);
            startActivity(i);
        }else{
            goToTipoCadastro();
        }
    }

    private void goToTipoCadastro(){
        Intent intent = new Intent(GoogleLoginActivity.this, TipoCadastroActivity.class);
        startActivity(intent);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(GoogleLoginActivity.this, R.string.conection_erro, Toast.LENGTH_LONG).show();
    }
}
