package com.example.samsung.tcc.classes;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.example.samsung.tcc.usuario.cadastro.login.LoginActivity;

import java.util.HashMap;

/**
 * Created by Rebeca de Melo on 13/04/2017.
 */
public class SessionManager {

    SharedPreferences preferences;
    Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    private static final String PREFERENCE_NAME = "InfoUsuario";
    public static final String IS_LOGGED = "Logged";
    public static final String KEY_CODU = "codU";
    public static final String KEY_NOME = "nome";
    public static final String KEY_DATA = "data";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_TOKEN = "token";

    public SessionManager(Context context){
        this._context = context;
        preferences = _context.getSharedPreferences(PREFERENCE_NAME, PRIVATE_MODE);
        editor = preferences.edit();
    }

    public void loginSession(int codU, String nome, String data, String email, String token){
        editor.putBoolean(IS_LOGGED, true);
        editor.putInt(KEY_CODU, codU);
        editor.putString(KEY_NOME, nome);
        editor.putString(KEY_DATA, data);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_TOKEN, token);
        editor.commit();
    }

    public HashMap<String, String> getInfoUsuario(){
        HashMap<String, String> usuario = new HashMap<>();

        usuario.put(KEY_CODU, String.valueOf(preferences.getInt(KEY_CODU, 0))); // preferences.getString(KEY_CODU, null) String.valueOf(preferences.getInt(KEY_CODU, Integer.parseInt(null))));
        usuario.put(KEY_NOME, preferences.getString(KEY_NOME, null));
        usuario.put(KEY_DATA, preferences.getString(KEY_DATA, null));
        usuario.put(KEY_EMAIL, preferences.getString(KEY_EMAIL, null));
        usuario.put(KEY_TOKEN, preferences.getString(KEY_TOKEN, null));

        return usuario;
    }

    public void checkLogin(){
        if(!this.isLoggedIn()){
            Intent intent = new Intent(_context, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(intent);
        }
    }

    public boolean isLoggedIn(){
        return preferences.getBoolean(IS_LOGGED, false);
    }

    public void logoutUsuario(){
        editor.clear();
        editor.commit();

        Intent intent = new Intent(_context, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(intent);
    }
}
