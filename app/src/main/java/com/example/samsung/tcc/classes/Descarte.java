package com.example.samsung.tcc.classes;

import java.util.ArrayList;

/**
 * Created by Rebeca de Melo on 21/05/2017.
 */
public class Descarte {
    private int codD;
    private Usuario usuario;
    private PontoColeta pontoColeta;
    private String dataD;
    private boolean statusD;
    private ArrayList<Residuo> residuos;

    public Descarte(int codD, Usuario usuario, PontoColeta pontoColeta, String dataD, boolean statusD, ArrayList<Residuo> residuos) {
        this.codD = codD;
        this.usuario = usuario;
        this.pontoColeta = pontoColeta;
        this.dataD = dataD;
        this.statusD = statusD;
        this.residuos = residuos;
    }

    public Descarte() {
    }

    public int getCodD() {
        return codD;
    }

    public void setCodD(int codD) {
        this.codD = codD;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public PontoColeta getPontoColeta() {
        return pontoColeta;
    }

    public void setPontoColeta(PontoColeta pontoColeta) {
        this.pontoColeta = pontoColeta;
    }

    public String getDataD() {
        return dataD;
    }

    public void setDataD(String dataD) {
        this.dataD = dataD;
    }

    public boolean isStatusD() {
        return statusD;
    }

    public void setStatusD(boolean statusD) {
        this.statusD = statusD;
    }

    public ArrayList<Residuo> getResiduos() {
        return residuos;
    }

    public void setResiduos(ArrayList<Residuo> residuos) {
        this.residuos = residuos;
    }
}
