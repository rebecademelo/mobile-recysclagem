package com.example.samsung.tcc.drawer;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
//import android.support.v7.widget.SearchView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.samsung.tcc.classes.Residuo;
import com.example.samsung.tcc.R;

import java.io.IOException;
import java.util.ArrayList;

import com.example.samsung.tcc.classes.SessionManager;
import com.example.samsung.tcc.usuario.ContaFragment;
import com.example.samsung.tcc.pontocoleta.MapaFragment;
import com.example.samsung.tcc.descarte.MeusDescartesFragment;
import com.example.samsung.tcc.usuario.MeusFavoritosFragment;
import com.example.samsung.tcc.pontocoleta.SolicitarPontoFragment;
import com.example.samsung.tcc.residuo.ListaCardResiduoActivity;
import com.example.samsung.tcc.residuo.RecyclerViewAdapter;
import com.example.samsung.tcc.utilities.PropertySource;

public class DrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    private Toolbar toolbar;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;
    SessionManager session;
    // index para identificar o item atual selecionado na drawer
    private static int navItemIndex = 0;

    //properties
    public static boolean WS_PROD = false;
    public static String WS_URL = "10.0.3.2";

    // tags dos fragmentos
    private static final String TAG_RESIDUOS = "residuos";
    private static final String TAG_CONTA = "conta";
    private static final String TAG_MAPA = "mapa";
    private static final String TAG_FAVORITOS = "favoritos";
    private static final String TAG_DESCARTES = "descartes";
    private static final String TAG_SOLICITACAOPONTO = "solicitacao";
    private static final String TAG_TUTORIALUSOAPP = "tutorialApp";
    public static String TAG_ATUAL = TAG_RESIDUOS;
    // titulos do toolbar que mudam dependendo da activity escolhida
    private String[] toolbarTitulos;
    // carrega a activity principal quando onBackPressed
    private boolean carregaResiduosOnBackPressed = true;
    private Handler handler;
    //
    private RecyclerViewAdapter adapter;
    ArrayList<Residuo> infoResiduos;

    public static ArrayList<Residuo> selectedResiduos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        try {
            DrawerActivity.WS_PROD =  Boolean.valueOf(
                    PropertySource.getProperty("resysclagem.service.prod", getApplicationContext())
            );
            if(DrawerActivity.WS_PROD){
                DrawerActivity.WS_URL = PropertySource.getProperty("resysclagem.service.host", getApplicationContext());
            } else {
                DrawerActivity.WS_URL = PropertySource.getProperty("resysclagem.service.localhost", getApplicationContext());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        session = new SessionManager(getApplicationContext());
        session.checkLogin();

        handler = new Handler();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbarTitulos = getResources().getStringArray(R.array.nav_toolbar_titulo);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        //drawer.setDrawerListener(toggle);
        drawer.addDrawerListener(toggle);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if(savedInstanceState == null){
            navItemIndex = 0;
            TAG_ATUAL = TAG_RESIDUOS;
            carregaFragment();
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(toggle.onOptionsItemSelected(item)){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void carregaFragment(){ // carrega a activity/fragment que foi escolhida na drawer
        selecionaNavMenuItem();
        setToolbarTitulo();
        // se o usuario selecionar o item da drawer atual nao acontecera nada, a drawer só fechara
        if(getSupportFragmentManager().findFragmentByTag(TAG_ATUAL) != null){
            drawer.closeDrawers();
            return;
        }

        Runnable fragmentRunnable = new Runnable() {
            @Override
            public void run() {
                // atualiza o conteudo por fragments
                Fragment fragment = getListaResiduoFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frameContent, fragment, TAG_ATUAL);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        if(fragmentRunnable != null){
            handler.post(fragmentRunnable);
        }

        drawer.closeDrawers();
    }

    private Fragment getListaResiduoFragment(){
        switch (navItemIndex){
            case 0:
                ListaCardResiduoActivity listaFragment = new ListaCardResiduoActivity();
                return listaFragment;
            case 1:
                ContaFragment contaFragment = new ContaFragment();
                return contaFragment;
            case 2:
                MapaFragment mapaFragment = new MapaFragment();
                return mapaFragment;
            case 3:
                MeusFavoritosFragment favoritosFragment = new MeusFavoritosFragment();
                return favoritosFragment;
            case 4:
                MeusDescartesFragment descartesFragment = new MeusDescartesFragment();
                return descartesFragment;
            case 5:
                SolicitarPontoFragment solicitarFragment = new SolicitarPontoFragment();
                return solicitarFragment;
            case 6:
                // a fazer // tutorial de uso do app
            default:
                return new ListaCardResiduoActivity();
        }
    }

    private void setToolbarTitulo(){
        getSupportActionBar().setTitle(toolbarTitulos[navItemIndex]);
    }

    private void selecionaNavMenuItem(){ // seleciona o item da drawer certo
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_residuos:
                navItemIndex = 0;
                TAG_ATUAL = TAG_RESIDUOS;
                break;
            case R.id.nav_conta:
                navItemIndex = 1;
                TAG_ATUAL = TAG_CONTA;
                break;
            case R.id.nav_mapa:
                navItemIndex = 2;
                TAG_ATUAL = TAG_MAPA;
                break;
            case R.id.nav_favoritos:
                navItemIndex = 3;
                TAG_ATUAL = TAG_FAVORITOS;
                break;
            case R.id.nav_descartes:
                navItemIndex = 4;
                TAG_ATUAL = TAG_DESCARTES;
                break;
            case R.id.nav_solicitacaoPonto:
                navItemIndex = 5;
                TAG_ATUAL = TAG_SOLICITACAOPONTO;
                break;
            case R.id.nav_tutorialUsoApp:
                navItemIndex = 6;
                TAG_ATUAL = TAG_TUTORIALUSOAPP;
                break;
            default:
                navItemIndex = 0;
        }

        if(item.isChecked()){
            item.setChecked(false);
        }else {
            item.setChecked(true);
        }
        item.setChecked(true);

        carregaFragment();

        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
        }
        // carrega a lista de residuos(tela principal) quando a back key é pressionada quando o usuario esta em uma atividade que nao é a lista
        if(carregaResiduosOnBackPressed){
            // checa se o usuario esta em outro item do menu de navegacao que nao é a lista
            if(navItemIndex != 0){
                navItemIndex = 0;
                TAG_ATUAL = TAG_RESIDUOS;
                carregaFragment();
                return;
            }
        }
        super.onBackPressed();
    }
}
