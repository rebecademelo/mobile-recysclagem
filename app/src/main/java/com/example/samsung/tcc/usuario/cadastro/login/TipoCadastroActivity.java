package com.example.samsung.tcc.usuario.cadastro.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.samsung.tcc.R;
import com.example.samsung.tcc.classes.ContaUsuario;
import com.example.samsung.tcc.classes.SessionManager;
import com.example.samsung.tcc.classes.Usuario;
import com.example.samsung.tcc.utilities.CustomJSONObjectRequest;
import com.example.samsung.tcc.utilities.PathClass;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

public class TipoCadastroActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{
    private ProgressDialog mProgressDialog;
    private Gson gson = new Gson();
    private Usuario usuario;
    private String id;
    private String nome;
    private String email;
    private String senha;
    private SessionManager session;
    //request
    private com.example.samsung.tcc.classes.Request request;
    private PathClass path;
    private String url = "";
    private JSONObject requestObj;
    // google
    private SignInButton googleSignIn;
    private int RC_SIGN_IN = 0;
    private GoogleApiClient mGoogleApiClient;
    //facebook
    private CallbackManager callbackManager;
    private LoginButton facebookLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_cadastro);

        session = new SessionManager(getApplicationContext());

        // GOOGLE
        // obtem as informações do usuario
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // cria um GoogleApiClient com acesso a API Google Sign-In
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener*/ )
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        googleSignIn = (SignInButton) findViewById(R.id.btGoogle);

        googleSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                //Toast.makeText(TipoCadastroActivity.this, "OnCreate", Toast.LENGTH_LONG).show(); // 4
            }
        });

        // FACEBOOK
        callbackManager = CallbackManager.Factory.create();
        facebookLogin = (LoginButton) findViewById(R.id.btFacebook);

        facebookLogin.setReadPermissions(Arrays.asList("email", "public_profile", "user_birthday"));

        facebookLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                goToFacebookLogin();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), getString(R.string.login_cancelado) , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getApplicationContext(), getString(R.string.facebook_login_erro) + " " + error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // resultado retornado da intent --> GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Logado com sucesso
            GoogleSignInAccount acct = result.getSignInAccount();
            id = acct.getId();
            nome = acct.getDisplayName();
            email = acct.getEmail();
            senha = "g" + id;
            //String url = "http://10.0.3.2:8008/server/usuario/cadastro";
            //String urlEmail = "http://10.0.3.2:8008/server/usuario?email="+email;
            path = new PathClass(this);
            try {
                url = path.getServerPath() + "/usuario?email="+email;
            } catch (IOException e) {
                e.printStackTrace();
            }
            // google sign in
            CustomJSONObjectRequest stringRequest = new CustomJSONObjectRequest(Request.Method.GET, /*urlEmail*/url, null, getBaseContext(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            ContaUsuario contaUsuario = (ContaUsuario) gson.fromJson(response.toString(), ContaUsuario.class);
                            com.example.samsung.tcc.classes.Request usuarioRequest;
                            if(contaUsuario.getLoginExists()){
                                usuarioRequest = contaUsuario.getRequest();
                                Usuario usuarioLogado = gson.fromJson(usuarioRequest.getObjeto(), Usuario.class);
                                session.loginSession(usuarioLogado.getCodU(), nome, usuarioLogado.getDatanasc(), email, usuarioRequest.getToken());
                                goToGoogleLogin();
                            }else {
                                Bundle bundle = new Bundle();
                                bundle.putString("id", id);
                                bundle.putString("nome", nome);
                                bundle.putString("email", email);
                                Intent intent = new Intent(TipoCadastroActivity.this, GetDatanascActivity.class);
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                if (error.networkResponse.statusCode != 0) {
                                    Toast.makeText(TipoCadastroActivity.this, R.string.mensagem_erro_response + " " + error.networkResponse.statusCode, Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(TipoCadastroActivity.this, R.string.mensagem_erro_get + " " + error.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            } catch(Exception e){
                                Toast.makeText(TipoCadastroActivity.this, R.string.exception_mensagem + " " + error.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }else{
            Toast.makeText(TipoCadastroActivity.this, R.string.google_login_erro, Toast.LENGTH_LONG).show();
            Log.e("google",  result.getStatus().toString());
        }
    }

    private void goToGoogleLogin(){
        Intent intent = new Intent(TipoCadastroActivity.this, GoogleLoginActivity.class);
        startActivity(intent);
    }

    private void goToFacebookLogin() {
        Intent usuarioFacebook = new Intent(this, FacebookLoginActivity.class);
        startActivity(usuarioFacebook);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(TipoCadastroActivity.this, getString(R.string.conection_erro) + " " + connectionResult, Toast.LENGTH_LONG).show(); // 2, 5
    }

    public void email(View view) { // email normal
        Intent i = new Intent(TipoCadastroActivity.this, CadastroEmailActivity.class);
        startActivity(i);
    }
}
