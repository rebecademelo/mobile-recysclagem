package com.example.samsung.tcc.classes;

/**
<<<<<<< Updated upstream
 * Created by rodtw on 14/04/2017.
 */

public class ContaUsuario {
    private boolean loginExists;
    private boolean passwordConfirmed;
    private Request request;

    public ContaUsuario(boolean loginExists, boolean passwordConfirmed, Request request){
        this.loginExists = loginExists;
        this.passwordConfirmed = passwordConfirmed;
        this.request = request;
    }

    public ContaUsuario(){
    }

    public boolean getLoginExists() {
        return loginExists;
    }

    public void setLoginExists(boolean loginExists) {
        this.loginExists = loginExists;
    }

    public boolean getPasswordConfirmed() {
        return passwordConfirmed;
    }

    public void setPasswordConfirmed(boolean passwordConfirmed) {
        this.passwordConfirmed = passwordConfirmed;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
}
