package com.example.samsung.tcc.descarte;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.samsung.tcc.classes.VolleyCallback;
import com.example.samsung.tcc.drawer.DrawerActivity;
import com.example.samsung.tcc.utilities.CheckHost;
import com.example.samsung.tcc.utilities.CustomJsonArrayRequest;
import com.example.samsung.tcc.R;
import com.example.samsung.tcc.classes.Descarte;
import com.example.samsung.tcc.classes.SessionManager;
import com.example.samsung.tcc.classes.Usuario;
import com.example.samsung.tcc.utilities.PathClass;
import com.example.samsung.tcc.utilities.PropertySource;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.samsung.tcc.pontocoleta.MapaFragment.PERMISSIONS_REQUEST_LOCATION;

public class MeusDescartesFragment extends Fragment implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private SessionManager session;
    private int codU;
    private String nomeU;
    private String datanascU;
    private String emailU;
    private String token;
    private Usuario usuario;
    private Gson gson = new Gson();
    private ProgressDialog pDialog;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private DescarteRecyclerViewAdapter adapter;
    private ArrayList<Descarte> listaDescarte;
    private com.example.samsung.tcc.classes.Request request;
    private PathClass path;
    private String url = "";
    private JSONObject requestObj;
    private CheckHost host = new CheckHost();
    private LocationManager locationManager;
    private Location loc;
    private GoogleApiClient googleApiClient;
    private Location lastLocation;
    private LocationRequest locationRequest;
    public static final int PERMISSIONS_REQUEST_LOCATION = 99;
    private GoogleMap map;
    private MapView mapView;

    //private OnFragmentInteractionListener mListener;

    public MeusDescartesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkLocationPermission();

        //locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        session = new SessionManager(getContext());
        session.checkLogin();

        try {
            DrawerActivity.WS_PROD =  Boolean.valueOf(
                    PropertySource.getProperty("resysclagem.service.prod", getContext())
            );
            if(DrawerActivity.WS_PROD){
                DrawerActivity.WS_URL = PropertySource.getProperty("resysclagem.service.host", getContext());
            } else {
                DrawerActivity.WS_URL = PropertySource.getProperty("resysclagem.service.localhost", getContext());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        HashMap<String, String> usuarioLogado = session.getInfoUsuario();
        codU = Integer.parseInt(usuarioLogado.get(SessionManager.KEY_CODU));
        nomeU = usuarioLogado.get(SessionManager.KEY_NOME);
        datanascU = usuarioLogado.get(SessionManager.KEY_DATA);
        emailU = usuarioLogado.get(SessionManager.KEY_EMAIL);
        token = usuarioLogado.get(SessionManager.KEY_TOKEN);

        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        pDialog.show();

        Thread listaDescarteThread = new Thread(new Runnable() {
            @Override
            public void run() {
                if(host.isHostReachable(DrawerActivity.WS_URL, 8008, 5000)){
                    carregaListaDescarte(new VolleyCallback() {
                        @Override
                        public void onSuccess(JSONArray response) {
                            //hideProgressDialog();
                            Type listType = new TypeToken<ArrayList<Descarte>>() {
                            }.getType();
                            listaDescarte = gson.fromJson(response.toString(), listType);

                            //locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

                                //location = getLastLocation();
                            if(lastLocation != null){
                                Log.i("lastLocation", lastLocation.toString());
                            }
                            adapter = new DescarteRecyclerViewAdapter(getActivity(), listaDescarte, lastLocation);
                            recyclerView.setAdapter(adapter);
                            recyclerView.setLayoutManager(layoutManager);


                        }

                        @Override
                        public void onSuccess(JSONObject response) {

                        }

                        @Override
                        public void onError(VolleyError error) {
                            final VolleyError volleyError = error;
                            try {
                                if (error.networkResponse.statusCode != 0) {
                                    hideProgressDialog();
                                    Log.i("statusCode", String.valueOf(error.networkResponse.statusCode));
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),
                                                    "Ocorreu um problema: " + volleyError.networkResponse.statusCode,
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }
                                    });
                                } else if (error instanceof NetworkError) {
                                    hideProgressDialog();
                                    Log.i("networkError", error.getMessage());
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),
                                                    "Network Error: " + volleyError.getMessage(),
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                    });
                                } else if (error instanceof ServerError) {
                                    hideProgressDialog();
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),
                                                    "Server Error: " + volleyError.getMessage(),
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                    });
                                } else if (error instanceof AuthFailureError) {
                                    Log.i("authError", error.getMessage());
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),
                                                    "AuthFailure Error: " + volleyError.getMessage(),
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                    });
                                } else if (error instanceof ParseError) {
                                    hideProgressDialog();
                                    Log.i("parseError", error.getMessage());
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),
                                                    "Parse Error: " + volleyError.getMessage(),
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                    });
                                } else if (error instanceof NoConnectionError) {
                                    hideProgressDialog();
                                    Log.i("noconnectionError", error.getMessage());
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),
                                                    "NoConnection Error: " + volleyError.getMessage(),
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                    });
                                } else if (error instanceof TimeoutError) {
                                    hideProgressDialog();
                                    Log.i("timeoutError", error.getMessage());
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),
                                                    "Timeout Error: " + volleyError.getMessage(),
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                    });
                                } else {
                                    Log.i("else", error.getMessage());
                                    hideProgressDialog();
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),
                                                    "Ocorreu um problema: " + volleyError.getMessage(),
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                    });
                                }
                            } catch (final Exception e) {
                                Log.i("exececao", error.getMessage() + " & " + e.getMessage());
                                hideProgressDialog();
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getActivity(),
                                                "Ocorreu uma exceção: " + e.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    }

                                });
                            }
                        }
                    });
                } else {
                    hideProgressDialog();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(),
                                    "Não foi possíverl conectar ao servidor. Por favor, tente mais tarde.",
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }
            }
        });

        listaDescarteThread.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meus_descartes, container, false);

        mapView = (MapView) view.findViewById(R.id.latlongMap);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        recyclerView = (RecyclerView) view.findViewById(R.id.descarteRecycler);
        layoutManager = new LinearLayoutManager(getActivity());
        //adapter.notifyDataSetChanged();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void carregaListaDescarte(final VolleyCallback callback){
        path = new PathClass(getContext());
        try {
            url = path.getServerPath() + "/descarte/dousuario";
            usuario = new Usuario(codU, nomeU, emailU, datanascU);
            request = new com.example.samsung.tcc.classes.Request(gson.toJson(usuario), emailU, token);
            requestObj = new JSONObject(gson.toJson(request));

            CustomJsonArrayRequest listaDescarteReq = new CustomJsonArrayRequest(Request.Method.POST , url, requestObj, getContext(),
                    new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        callback.onSuccess(response);
                    }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    callback.onError(error);
                }
            });
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(listaDescarteReq);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void hideProgressDialog(){
        if(pDialog.isShowing()){
            pDialog.dismiss();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        lastLocation = location;
        hideProgressDialog();
        if(lastLocation != null){
            Log.i("lastLocation", lastLocation.toString());
        }
        adapter = new DescarteRecyclerViewAdapter(getActivity(), listaDescarte, lastLocation);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        //adapter.notifyDataSetChanged();
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        Log.i("latLngChanged", latLng.toString());
        //para atualizações de local
        if (googleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        }
    }

    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST_LOCATION);
            } else {
                // faz request da permissao
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    private Location getLastLocation(){
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for(String provider : providers){
            if(checkLocationPermission()){
                Location local = locationManager.getLastKnownLocation(provider);
                Log.i("local", local.toString());
                if(local != null){
                    if (bestLocation == null || local.getAccuracy() < bestLocation.getAccuracy()){
                        Log.i("bestLocation", local.toString());
                        bestLocation = local;
                    }
                }
            }
        }
        if(bestLocation == null){
            return null;
        }
        return bestLocation;
    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    protected void startLocationUpdates(){
        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(1500)
                .setFastestInterval(5000);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnected(Bundle bundle) {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        buildGoogleApiClient();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i("onConnectionFailed", "Erro: " + connectionResult.getErrorMessage() + ". Error code: " + connectionResult.getErrorCode());
    }

    @Override
    public void onStop(){
        super.onStop();
        if(googleApiClient.isConnected()){
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_LOCATION: {
                // se o request é cancelado, o array de resultados fica vazio
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(getContext(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (googleApiClient == null) {
                            buildGoogleApiClient();
                        }
                    }
                } else {
                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(getContext(), R.string.permissao_negada, Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                map.setMyLocationEnabled(true);
                hideProgressDialog();
            }
        } else {
            buildGoogleApiClient();
            map.setMyLocationEnabled(true);
            hideProgressDialog();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mapView!=null){
            mapView.onResume();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mapView!=null){
            mapView.onDestroy();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if(mapView!=null){
            mapView.onLowMemory();
        }
    }
}
