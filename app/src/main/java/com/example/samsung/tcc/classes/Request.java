package com.example.samsung.tcc.classes;

/**
 * Created by rodtw on 02/06/2017.
 */

public class Request {
    private String objeto;	// Objeto qlqr em JSON
    private String id;		// email do usuario/admin autorizado
    private String token;

    public Request(String objeto, String id, String token) {
        this.objeto = objeto;
        this.id = id;
        this.token = token;
    }

    public String getObjeto() {
        return objeto;
    }

    public void setObjeto(String objeto) {
        this.objeto = objeto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}