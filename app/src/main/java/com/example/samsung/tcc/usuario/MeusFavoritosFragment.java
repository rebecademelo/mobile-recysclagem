package com.example.samsung.tcc.usuario;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.samsung.tcc.classes.VolleyCallback;
import com.example.samsung.tcc.drawer.DrawerActivity;
import com.example.samsung.tcc.pontocoleta.MapaFragment;
import com.example.samsung.tcc.utilities.CheckHost;
import com.example.samsung.tcc.utilities.CustomJsonArrayRequest;
import com.example.samsung.tcc.R;
import com.example.samsung.tcc.classes.Residuo;
import com.example.samsung.tcc.classes.SessionManager;
import com.example.samsung.tcc.classes.Usuario;
import com.example.samsung.tcc.utilities.PathClass;
import com.example.samsung.tcc.utilities.PropertySource;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class MeusFavoritosFragment extends Fragment {
    private SessionManager session;
    private int codU;
    private String nomeU;
    private String datanascU;
    private String emailU;
    private String token;
    private Usuario usuario;
    private Gson gson = new Gson();
    private ProgressDialog pDialog;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FavoritosRecyclerViewAdapter adapter;
    private ArrayList<Residuo> listaResiduosFav;
    private com.example.samsung.tcc.classes.Request request;
    private PathClass path;
    private String url = "";
    private JSONObject requestObj;
    private CheckHost host = new CheckHost();

    //private OnFragmentInteractionListener mListener;

    public MeusFavoritosFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        session = new SessionManager(getContext());
        session.checkLogin();

        try {
            DrawerActivity.WS_PROD =  Boolean.valueOf(
                    PropertySource.getProperty("resysclagem.service.prod", getContext())
            );
            if(DrawerActivity.WS_PROD){
                DrawerActivity.WS_URL = PropertySource.getProperty("resysclagem.service.host", getContext());
            } else {
                DrawerActivity.WS_URL = PropertySource.getProperty("resysclagem.service.localhost", getContext());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        HashMap<String, String> usuarioLogado = session.getInfoUsuario();
        codU = Integer.parseInt(usuarioLogado.get(SessionManager.KEY_CODU));
        nomeU = usuarioLogado.get(SessionManager.KEY_NOME);
        datanascU = usuarioLogado.get(SessionManager.KEY_DATA);
        emailU = usuarioLogado.get(SessionManager.KEY_EMAIL);
        token = usuarioLogado.get(SessionManager.KEY_TOKEN);


        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.show();

        Thread listaFavThread = new Thread(new Runnable() {
            @Override
            public void run() {
                if(host.isHostReachable(DrawerActivity.WS_URL, 8008, 5000)){
                    carregaListaResiduosFav(new VolleyCallback() {
                        @Override
                        public void onSuccess(JSONArray response) {
                            hideProgressDialog();
                            Type listType = new TypeToken<ArrayList<Residuo>>(){}.getType();
                            listaResiduosFav = gson.fromJson(response.toString(), listType);

                            adapter = new FavoritosRecyclerViewAdapter(getActivity(), listaResiduosFav);
                            recyclerView.setAdapter(adapter);
                            recyclerView.setLayoutManager(layoutManager);
                        }

                        @Override
                        public void onSuccess(JSONObject response) {

                        }

                        @Override
                        public void onError(VolleyError error) {
                            final VolleyError volleyError = error;
                            try {
                                if (error.networkResponse.statusCode != 0) {
                                    hideProgressDialog();
                                    Log.i("statusCode", String.valueOf(error.networkResponse.statusCode));
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),
                                                    "Ocorreu um problema: " + volleyError.networkResponse.statusCode,
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }
                                    });
                                } else if (error instanceof NetworkError) {
                                    hideProgressDialog();
                                    Log.i("networkError", error.getMessage());
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),
                                                    "Network Error: " + volleyError.getMessage(),
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                    });
                                } else if (error instanceof ServerError) {
                                    hideProgressDialog();
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),
                                                    "Server Error: " + volleyError.getMessage(),
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                    });
                                } else if (error instanceof AuthFailureError) {
                                    Log.i("authError", error.getMessage());
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),
                                                    "AuthFailure Error: " + volleyError.getMessage(),
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                    });
                                } else if (error instanceof ParseError) {
                                    hideProgressDialog();
                                    Log.i("parseError", error.getMessage());
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),
                                                    "Parse Error: " + volleyError.getMessage(),
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                    });
                                } else if (error instanceof NoConnectionError) {
                                    hideProgressDialog();
                                    Log.i("noconnectionError", error.getMessage());
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),
                                                    "NoConnection Error: " + volleyError.getMessage(),
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                    });
                                } else if (error instanceof TimeoutError) {
                                    hideProgressDialog();
                                    Log.i("timeoutError", error.getMessage());
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),
                                                    "Timeout Error: " + volleyError.getMessage(),
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                    });
                                } else {
                                    Log.i("else", error.getMessage());
                                    hideProgressDialog();
                                    Log.i("erroNull", volleyError.getMessage());
                                    /*getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),
                                                    "Ocorreu um problema: " + volleyError.getMessage(),
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                    });*/
                                }
                            } catch (final Exception e) {
                                Log.i("exececao", error.getMessage() + " & " + e.getMessage());
                                hideProgressDialog();
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getActivity(),
                                                "Ocorreu uma exceção: " + e.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    }

                                });
                            }
                        }
                    });
                } else {
                    hideProgressDialog();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(),
                                    "Não foi possíverl conectar ao servidor. Por favor, tente mais tarde.",
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }
            }
        });

        listaFavThread.start();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meus_favoritos, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.favoritosRecycler);
        layoutManager = new LinearLayoutManager(getActivity());

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void carregaListaResiduosFav(final VolleyCallback callback){
        path = new PathClass(getContext());
        try {
            url = path.getServerPath() + "/usuario/favoritos";
            usuario = new Usuario(codU, nomeU, emailU, datanascU);
            request = new com.example.samsung.tcc.classes.Request(gson.toJson(usuario), emailU, token);
            requestObj = new JSONObject(gson.toJson(request));

            CustomJsonArrayRequest listaFavoritoReq = new CustomJsonArrayRequest(Request.Method.POST , url, requestObj, getContext(),
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            callback.onSuccess(response);
                            //adapter.notifyDataSetChanged();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            callback.onError(error);
                        }
                    });
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(listaFavoritoReq);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater){
        menu.clear();
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.favmaps, menu);

        MenuItem goToMapa = menu.findItem(R.id.mapaToolbar);
        goToMapa.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                ((DrawerActivity) getActivity()).selectedResiduos = adapter.getResiduosSelecionados();

                Fragment fragment = new MapaFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frameContent, fragment);
                fragmentTransaction.commitAllowingStateLoss();

                return true;
            }
        });
    }

    public void hideProgressDialog(){
        if(pDialog.isShowing()){
            pDialog.dismiss();
        }
    }
}
