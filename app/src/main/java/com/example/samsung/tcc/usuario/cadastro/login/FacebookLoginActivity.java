package com.example.samsung.tcc.usuario.cadastro.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.samsung.tcc.R;
import com.example.samsung.tcc.classes.ContaUsuario;
import com.example.samsung.tcc.classes.MsgResponse;
import com.example.samsung.tcc.classes.SessionManager;
import com.example.samsung.tcc.classes.Usuario;
import com.example.samsung.tcc.drawer.DrawerActivity;
import com.example.samsung.tcc.utilities.CustomJSONObjectRequest;
import com.example.samsung.tcc.utilities.PathClass;
import com.example.samsung.tcc.utilities.SHAHelper;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FacebookLoginActivity extends AppCompatActivity {
    /*private TextView nome;
    private TextView email;
    private TextView datanasc;*/
    private Usuario usuario;
    private Gson gson = new Gson();
    private String cod;
    private String nomeU;
    private String emailU;
    private String datanascU;
    private String senhaU;
    private SessionManager session;
    private PathClass path;
    private String url = "";
    private JSONObject requestObj;
    private com.example.samsung.tcc.classes.Request usuarioRequest;
    private ContaUsuario contaUsuario;
    private MsgResponse msgResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_facebook_login);

        session = new SessionManager(getApplicationContext());

        /*nome = (TextView) findViewById(R.id.nomeF);
        email = (TextView) findViewById(R.id.emailF);
        datanasc = (TextView) findViewById(R.id.dataF);*/

        if (AccessToken.getCurrentAccessToken() == null) {
            goTipoCadastro();
        }

        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                path = new PathClass(getApplicationContext());
                try {
                    Profile profile = Profile.getCurrentProfile();
                    cod = profile.getId();
                    nomeU = response.getJSONObject().getString("name");
                    emailU = response.getJSONObject().getString("email");
                    datanascU = response.getJSONObject().getString("birthday");
                    senhaU = "f" + cod;

                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                        Date dateFormat = sdf.parse(datanascU);
                        sdf = new SimpleDateFormat("dd/MM/yyyy");
                        datanascU = sdf.format(dateFormat);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    SHAHelper shaHelper = new SHAHelper();
                    String senhaFacebook = shaHelper.getSHA256(senhaU);

                    usuario = new Usuario(nomeU, datanascU, emailU, senhaFacebook);

                    url = path.getServerPath() + "/usuario?email="+emailU;

                    //String urlEmail = "http://10.0.3.2:8008/server/usuario?email="+emailU;
                    CustomJSONObjectRequest stringRequest = new CustomJSONObjectRequest(Request.Method.GET, url, null, getBaseContext(),
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    contaUsuario = (ContaUsuario) gson.fromJson(response.toString(), ContaUsuario.class);
                                    if(contaUsuario.getLoginExists()){
                                        usuarioRequest = contaUsuario.getRequest();
                                        Usuario usuarioLogado = gson.fromJson(usuarioRequest.getObjeto(), Usuario.class);
                                        session.loginSession(usuarioLogado.getCodU(), nomeU, datanascU, emailU, usuarioRequest.getToken());
                                        goToListaResiduos();
                                    } else {
                                        try {
                                            //String url = "http://10.0.3.2:8008/server/usuario/cadastro";
                                            url = path.getServerPath() + "/usuario/cadastro";
                                            JSONObject jsonObject = new JSONObject(gson.toJson(usuario));

                                            CustomJSONObjectRequest stringRequest = new CustomJSONObjectRequest(url, jsonObject, getBaseContext(),
                                                    new Response.Listener<JSONObject>() {
                                                        @Override
                                                        public void onResponse(JSONObject response) {
                                                            msgResponse = gson.fromJson(response.toString(), MsgResponse.class);
                                                            Log.e("msgRes", response.toString());
                                                            contaUsuario = gson.fromJson(msgResponse.getExtra(), ContaUsuario.class);
                                                            usuarioRequest = contaUsuario.getRequest();

                                                            Usuario usuarioCadastrado = gson.fromJson(usuarioRequest.getObjeto(), Usuario.class);
                                                            int codCU = usuarioCadastrado.getCodU();
                                                            String nome = usuarioCadastrado.getNome();
                                                            String datanasc = usuarioCadastrado.getDatanasc();
                                                            String email = usuarioCadastrado.getEmail();
                                                            session.loginSession(codCU, nome, datanasc, email, usuarioRequest.getToken());
                                                            goToListaResiduos();
                                                        }
                                                    },
                                                    new Response.ErrorListener() {
                                                        @Override
                                                        public void onErrorResponse(VolleyError error) {
                                                            try {
                                                                if (error.networkResponse.statusCode != 0) {
                                                                    Toast.makeText(FacebookLoginActivity.this, R.string.mensagem_erro_response + " " + error.networkResponse.statusCode, Toast.LENGTH_LONG).show();
                                                                } else {
                                                                    Toast.makeText(FacebookLoginActivity.this, R.string.mensagem_erro_get + " " + error.getMessage(), Toast.LENGTH_LONG).show();
                                                                }
                                                            } catch(Exception e){
                                                                Toast.makeText(FacebookLoginActivity.this, R.string.exception_mensagem + " " + error.getMessage(), Toast.LENGTH_LONG).show();
                                                            }
                                                        }
                                                    });
                                            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                                            requestQueue.add(stringRequest);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            Toast.makeText(FacebookLoginActivity.this, R.string.json_facebook_criar + " " + e, Toast.LENGTH_LONG).show(); // 2, 5
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    try {
                                        if (error.networkResponse.statusCode != 0) {
                                            Toast.makeText(FacebookLoginActivity.this, R.string.mensagem_erro_response + " " + error.networkResponse.statusCode, Toast.LENGTH_LONG).show();
                                        } else {
                                            Toast.makeText(FacebookLoginActivity.this,R.string.mensagem_erro_get + " " + error.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    } catch(Exception e){
                                        Toast.makeText(FacebookLoginActivity.this, R.string.exception_mensagem + " " + error.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);
                }catch (JSONException e){
                    e.printStackTrace();
                    Toast.makeText(FacebookLoginActivity.this,R.string.facebook_erro_busca, Toast.LENGTH_LONG).show(); // 2, 5
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle b = new Bundle();
        b.putString("fields", "id ,name, email, birthday");
        request.setParameters(b);
        request.executeAsync();
    }

    private void goToListaResiduos(){
        Intent intent = new Intent(this, DrawerActivity.class);
        startActivity(intent);
    }

    private void goTipoCadastro() {
        Intent intent = new Intent(this, TipoCadastroActivity.class);
        startActivity(intent);
    }

    public void logout(View view) {
        LoginManager.getInstance().logOut();
        goTipoCadastro();
    }
}
