package com.example.samsung.tcc.utilities;

import android.content.Context;

import com.example.samsung.tcc.drawer.DrawerActivity;

import java.io.File;
import java.io.IOException;

/**
 * Created by Rebeca de Melo on 03/05/2017.
 */
public class PathClass {
    private Context context;

    //constructor
    public PathClass(Context context){
        this.context = context;
    }

    private String getWsBasePath() throws IOException {
        StringBuilder birl = new StringBuilder();
        birl.append(PropertySource.getProperty("resysclagem.service.protocol", context));
        birl.append("://");
        boolean prod =  Boolean.valueOf(
                            PropertySource.getProperty("resysclagem.service.prod", context)
                        );
        if(prod) {
            birl.append(PropertySource.getProperty("resysclagem.service.host", context));
        } else {
            birl.append(PropertySource.getProperty("resysclagem.service.localhost", context));
        }
        birl.append(":");
        birl.append(PropertySource.getProperty("resysclagem.service.port", context));
        return birl.toString();
    }

    public String getServerPath() throws IOException {
        StringBuilder birl = new StringBuilder();
        birl.append(this.getWsBasePath());
        birl.append(PropertySource.getProperty("resysclagem.service.ws", context));
        return birl.toString();
    }

    public String getFilePath() throws IOException {
        StringBuilder birl = new StringBuilder();
        birl.append(this.getWsBasePath());
        birl.append(PropertySource.getProperty("resysclagem.service.file", context));
        return birl.toString();
    }

}
