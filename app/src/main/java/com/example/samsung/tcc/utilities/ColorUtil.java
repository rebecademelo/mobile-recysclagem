package com.example.samsung.tcc.utilities;

import android.graphics.Color;
import android.support.v4.graphics.ColorUtils;

/**
 * Created by rodtw on 07/10/2017.
 */

public class ColorUtil {

    public static boolean isDark (int color){
        if (getLightness(color) < 0.5f ){
            return true;
        }
        return false;
    }

    private static float getLightness(int color) {
        int red   = Color.red(color);
        int green = Color.green(color);
        int blue  = Color.blue(color);

        float hsl[] = new float[3];
        ColorUtils.RGBToHSL(red, green, blue, hsl);
        return hsl[2];
    }
}
