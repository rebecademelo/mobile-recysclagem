package com.example.samsung.tcc.descarte;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.samsung.tcc.R;
import com.example.samsung.tcc.classes.Descarte;
import com.example.samsung.tcc.classes.MsgResponse;
import com.example.samsung.tcc.classes.Residuo;
import com.example.samsung.tcc.classes.SessionManager;
import com.example.samsung.tcc.classes.Usuario;
import com.example.samsung.tcc.classes.UsuarioResiduo;
import com.example.samsung.tcc.classes.VolleyCallback;
import com.example.samsung.tcc.drawer.DrawerActivity;
import com.example.samsung.tcc.utilities.CheckHost;
import com.example.samsung.tcc.utilities.CustomJSONObjectRequest;
import com.example.samsung.tcc.utilities.PathClass;
import com.google.android.gms.location.LocationListener;
import com.google.gson.Gson;
import com.silencedut.expandablelayout.ExpandableLayout;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;

import static com.example.samsung.tcc.pontocoleta.MapaFragment.PERMISSIONS_REQUEST_LOCATION;

/**
 * Created by Rebeca de Melo on 29/05/2017.
 */
public class DescarteRecyclerViewAdapter extends RecyclerView.Adapter<DescarteRecyclerViewAdapter.ViewHolder> {
    private Context context;
    private HashSet<Integer> expandedPosition;
    private ArrayList<Descarte> descarteList = new ArrayList<>();
    private Gson gson = new Gson();
    private Usuario usuario;
    private PathClass path;
    private String url = "";
    private JSONObject requestObj;
    private com.example.samsung.tcc.classes.Request request;
    private SessionManager session;
    private String email;
    private String token;
    private Descarte descarteItem;
    private CheckHost host = new CheckHost();
    private LocationManager locationManager;
    private Location location;
    private ProgressDialog pDialog;

    public DescarteRecyclerViewAdapter(Context context, ArrayList<Descarte> descarteList, Location location){
        this.context = context;
        expandedPosition = new HashSet<>();
        this.descarteList = descarteList;
        this.location = location;
    }

    @Override
    public int getItemCount() {
        //return descarteList.size();
        return (null != descarteList ? descarteList.size() : 0);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.descarte_items, null);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.updateItem(position);
        descarteItem = descarteList.get(position);
        holder.data.setText(descarteItem.getDataD());
        holder.local.setText(descarteItem.getPontoColeta().getEndereco());
        String concText = "Resíduos: ";
        int i = 0;
        for (Residuo r : descarteItem.getResiduos()){
            if (r != null) {
                concText = concText + r.getNome();
                i++;
                if (i == descarteItem.getResiduos().size()) {
                    break;
                }
                concText = concText + "\n";
            }
        }
        holder.residuos.setText(concText);
        boolean statusDescarte = descarteItem.isStatusD();
        if(statusDescarte){
            Picasso.with(context).load(R.drawable.verde).into(holder.status);
        }else {
            Picasso.with(context).load(R.drawable.vermelho).into(holder.status);
        }
        holder.tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down_black_24px, 0);
        holder.tv.setText("Mostrar mais");
        if(descarteItem.isStatusD()){
            holder.validarDescarte.setEnabled(false);
        } else {
            holder.validarDescarte.setEnabled(true);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private ExpandableLayout expandableLayout;
        private TextView data;
        private TextView local;
        private TextView residuos;
        private ImageView status;
        private TextView tv;
        private Button validarDescarte;
        private CardView card;
        private int position;

        public ViewHolder(View view) {
            super(view);
            expandableLayout = (ExpandableLayout) view.findViewById(R.id.expandable_layout);
            data = (TextView) view.findViewById(R.id.dataItem);
            local = (TextView) view.findViewById(R.id.localItem);
            residuos = (TextView) view.findViewById(R.id.residuosItem);
            status = (ImageView) view.findViewById(R.id.imgStatusItem);
            tv = (TextView) view.findViewById(R.id.vermais_menosD);
            validarDescarte = (Button) view.findViewById(R.id.btnValidarDescarte);
            card = (CardView) view.findViewById(R.id.descateCard);

            validarDescarte.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pDialog = new ProgressDialog(context);
                    pDialog.setCancelable(false);
                    pDialog.show();
                    // pega a posicao do card clicado
                    position = getAdapterPosition();
                    descarteItem = descarteList.get(position);
                    Log.i("position", String.valueOf(position));

                    if(location != null){
                        Log.i("locationAdapter", String.valueOf(location));
                        double latitude = location.getLatitude();
                        double longitude = location.getLongitude();
                        Log.i("pendenteLocation", latitude + " " + longitude);

                        Location p = new Location("ponto");
                        p.setLatitude(descarteItem.getPontoColeta().getLatitude());
                        p.setLongitude(descarteItem.getPontoColeta().getLongitude());
                        Location c = new Location("currentlocation");
                        c.setLatitude(latitude);
                        c.setLongitude(longitude);

                        float distancia = p.distanceTo(c);
                        Log.i("distancia", String.valueOf(distancia));

                        descarteItem.setStatusD(true);
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        String dataDescarte = dateFormat.format(Calendar.getInstance().getTime());
                        descarteItem.setDataD(dataDescarte);

                        if(distancia <= 200.0){
                                validarDescarte(new VolleyCallback() {
                                    @Override
                                    public void onSuccess(JSONArray response) {

                                    }

                                    @Override
                                    public void onSuccess(JSONObject response) {
                                        MsgResponse msgResponse = gson.fromJson(response.toString(), MsgResponse.class);
                                        if(msgResponse.isStatus()){
                                            String message = msgResponse.getMessage();
                                            Log.i("message", message);
                                            Log.i("descarte", String.valueOf(descarteItem.getPontoColeta().getNome()));
                                            Picasso.with(context).load(R.drawable.verde).into(status);
                                            notifyItemChanged(position);
                                            hideProgressDialog();
                                            Toast.makeText(context,
                                                    "Seu descarte foi completado com sucesso!",
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        } else {
                                            String message = msgResponse.getMessage();
                                            Log.i("message", message);
                                            notifyItemChanged(position);
                                            hideProgressDialog();
                                            Toast.makeText(context,
                                                    "Não foi possível realizar seu descarte, por favor tente mais tarde!",
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }
                                    }

                                    @Override
                                    public void onError(VolleyError error) {
                                        if(error.getMessage() != null){
                                            Log.i("volleyErrorD", error.getMessage());
                                        }
                                    }
                                }, descarteItem);
                            } else {
                                hideProgressDialog();
                                ((DrawerActivity) context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(context,
                                                "É necessário estar no local do ponto de coleta para realizar o descarte!",
                                                Toast.LENGTH_LONG)
                                                .show();
                                    }
                                });
                            }
                    } else {
                        hideProgressDialog();
                        ((DrawerActivity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(context,
                                        "É necessário estar no local do ponto de coleta para realizar o descarte!",
                                        Toast.LENGTH_LONG)
                                        .show();
                            }
                        });
                        Log.i("locationNull", "Não foi possível encontrar a localização atual!");
                    }
                }
            });
        }

        private void updateItem(final int position){
            expandableLayout.setOnExpandListener(new ExpandableLayout.OnExpandListener() {
                @Override
                public void onExpand(boolean expanded) {
                    registerExpand(position, tv, validarDescarte);
                }
            });
            expandableLayout.setExpand(expandedPosition.contains(position));
        }

        private void registerExpand(int position, TextView tv, Button descartar){
            if(expandedPosition.contains(position)){
                removeExpand(position);
                tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down_black_24px, 0);
                tv.setText("Mostrar mais");
            } else {
                addExpand(position);
                tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_up_black_24px, 0);
                tv.setText("Mostrar menos");
                descarteItem = descarteList.get(position);

            }
        }

        private void removeExpand(int position) {
            expandedPosition.remove(position);
        }

        private void addExpand(int position) {
            expandedPosition.add(position);
        }
    }

    private void validarDescarte(final VolleyCallback callback, Descarte descarte){
        path = new PathClass(context);
        try {
            session = new SessionManager(context);
            session.checkLogin();

            HashMap<String, String> usuarioLogado = session.getInfoUsuario();
            token = usuarioLogado.get(SessionManager.KEY_TOKEN);
            email = usuarioLogado.get(SessionManager.KEY_EMAIL);

            //descarte.setStatusD(true);

            Log.i("descarteItem", String.valueOf(descarte.isStatusD()));

            url = path.getServerPath() + "/descarte";

            request = new com.example.samsung.tcc.classes.Request(gson.toJson(descarte), email, token);
            requestObj = new JSONObject(gson.toJson(request));
            Log.i("request", requestObj.toString());

            CustomJSONObjectRequest validarDescarteReq = new CustomJSONObjectRequest(Request.Method.PUT , url, requestObj, context,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            callback.onSuccess(response);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            callback.onError(error);
                        }
                    });
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(validarDescarteReq);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public boolean checkLocationPermission(){
        DrawerActivity dContext = (DrawerActivity) context;

        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(dContext,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {


                ActivityCompat.requestPermissions(dContext,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST_LOCATION);
            } else {
                // faz request da permissao
                ActivityCompat.requestPermissions(dContext,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    public void hideProgressDialog(){
        if(pDialog.isShowing()){
            pDialog.dismiss();
        }
    }
}
