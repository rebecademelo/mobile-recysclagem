package com.example.samsung.tcc.classes;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Rebeca de Melo on 29/04/2017.
 */
public class Dica implements Serializable{
    private int codD;
    private String nome;
    private String descricaoPath;
    private boolean isReutilizavel;
    private Residuo residuo;
    //private List<String> anexos;

    public Dica(int codD, String nome, String descricaoPath, boolean isReutilizavel, Residuo residuo/*, List<String> anexos*/) {
        this.codD = codD;
        this.nome = nome;
        this.descricaoPath = descricaoPath;
        this.isReutilizavel = isReutilizavel;
        this.residuo = residuo;
        //this.anexos = anexos;
    }

    public Dica() {
    }

    public int getCodD() {
        return codD;
    }

    public void setCodD(int codD) {
        this.codD = codD;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricaoPath() {
        return descricaoPath;
    }

    public void setDescricaoPath(String descricaoPath) {
        this.descricaoPath = descricaoPath;
    }

    public boolean isReutilizavel() {
        return isReutilizavel;
    }

    public void setReutilizavel(boolean reutilizavel) {
        isReutilizavel = reutilizavel;
    }

    public Residuo getResiduo() {
        return residuo;
    }

    public void setResiduo(Residuo residuo) {
        this.residuo = residuo;
    }

    /*public List<String> getAnexos() {
        return anexos;
    }

    public void setAnexos(List<String> anexos) {
        this.anexos = anexos;
    }*/
}
