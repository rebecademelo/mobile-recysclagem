package com.example.samsung.tcc.pontocoleta;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.samsung.tcc.R;
import com.example.samsung.tcc.classes.MsgResponse;
import com.example.samsung.tcc.classes.PontoColeta;
import com.example.samsung.tcc.classes.Residuo;
import com.example.samsung.tcc.classes.SessionManager;
import com.example.samsung.tcc.classes.Usuario;
import com.example.samsung.tcc.classes.VolleyCallback;
import com.example.samsung.tcc.classes.subcls.DiaSemana;
import com.example.samsung.tcc.classes.subcls.HoraAtend;
import com.example.samsung.tcc.utilities.CustomJSONObjectRequest;
import com.example.samsung.tcc.utilities.PathClass;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class SolicitarPontoFragment extends Fragment {
    private SessionManager session;
    private int codU;
    private String nomeU;
    private String datanascU;
    private String emailU;
    private Usuario usuario;
    private Gson gson = new Gson();
    private ProgressDialog pDialog;
    private PathClass path;
    private String url = "";
    private JSONObject requestObj;
    private com.example.samsung.tcc.classes.Request request;
    private String token;
    private EditText nomePonto;
    private EditText enderecoPonto;
    private EditText descricaoPonto;
    private EditText telefonePonto;
    private EditText emailPonto;
    //private EditText horarioAtendimentoPonto;
    private EditText msgPonto;
    private Button btnSolicitar;
    private AlertDialog alertDialog;
    private MsgResponse msgResponse;

    //private OnFragmentInteractionListener mListener;

    public SolicitarPontoFragment() {
    }

    /*public static SolicitarPontoFragment newInstance(String param1, String param2) {
        SolicitarPontoFragment fragment = new SolicitarPontoFragment();
        fragment.setArguments(args);
        return fragment;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*nomePonto = (EditText) getActivity().findViewById(R.id.nomeP);
        enderecoPonto = (EditText) getActivity().findViewById(R.id.enderecoP);
        descricaoPonto = (EditText) getActivity().findViewById(R.id.descricaoP);
        telefonePonto = (EditText) getActivity().findViewById(R.id.telefoneP);
        emailPonto = (EditText) getActivity().findViewById(R.id.emailP);
        horarioAtendimentoPonto = (EditText) getActivity().findViewById(R.id.horarioAtendimentoP);
        msgPonto = (EditText) getActivity().findViewById(R.id.msgRequisicao);
        btnSolicitar = (Button) getActivity().findViewById(R.id.btnSolicitar);*/

        session = new SessionManager(getContext());
        session.checkLogin();

        HashMap<String, String> usuarioLogado = session.getInfoUsuario();
        codU = Integer.parseInt(usuarioLogado.get(SessionManager.KEY_CODU));
        nomeU = usuarioLogado.get(SessionManager.KEY_NOME);
        datanascU = usuarioLogado.get(SessionManager.KEY_DATA);
        emailU = usuarioLogado.get(SessionManager.KEY_EMAIL);
        token = usuarioLogado.get(SessionManager.KEY_TOKEN);
        usuario = new Usuario(codU, nomeU, emailU, datanascU);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_solicitar_ponto, container, false);

        nomePonto = (EditText) view.findViewById(R.id.nomeP);
        enderecoPonto = (EditText) view.findViewById(R.id.enderecoP);
        descricaoPonto = (EditText) view.findViewById(R.id.descricaoP);
        telefonePonto = (EditText) view.findViewById(R.id.telefoneP);
        emailPonto = (EditText) view.findViewById(R.id.emailP);
        //horarioAtendimentoPonto = (EditText) view.findViewById(R.id.horarioAtendimentoP);
        msgPonto = (EditText) view.findViewById(R.id.msgRequisicao);
        btnSolicitar = (Button) view.findViewById(R.id.btnSolicitar);

        btnSolicitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDialog = new ProgressDialog(getContext());
                pDialog.setCancelable(false);
                pDialog.show();

                solicitarPonto(new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray response) {

                    }

                    @Override
                    public void onSuccess(JSONObject response) {
                        hideProgressDialog();
                        msgResponse = gson.fromJson(response.toString(), MsgResponse.class);
                        if(msgResponse.isStatus()){
                            Log.i("statusTrue", msgResponse.getMessage());
                            alertDialog = new AlertDialog.Builder(getContext()).create();
                            alertDialog.setTitle(getString(R.string.solicitacao_ponto_enviada)); //R.string.descarte
                            alertDialog.setMessage(getString(R.string.solicitacao_ponto_enviada_mensagem));
                            alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //Toast.makeText(getActivity().getApplicationContext(), "Solicitação realizada!", Toast.LENGTH_LONG).show();
                                    nomePonto.getText().clear();
                                    enderecoPonto.getText().clear();
                                    descricaoPonto.getText().clear();
                                    descricaoPonto.getText().clear();
                                    telefonePonto.getText().clear();
                                    emailPonto.getText().clear();
                                    //horarioAtendimentoPonto.getText().clear();
                                    msgPonto.getText().clear();
                                }
                            });
                            alertDialog.show();
                        }else {
                            hideProgressDialog();
                            Log.i("statusFalse", msgResponse.getMessage());
                            alertDialog = new AlertDialog.Builder(getActivity()).create();
                            alertDialog.setTitle(getString(R.string.solicitacao_ponto_nao_enviada)); //R.string.descarte
                            alertDialog.setMessage(getString(R.string.solicitacao_ponto_nao_enviada_mensagem));
                            alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(getContext(),
                                            msgResponse.getMessage(),
                                            Toast.LENGTH_LONG)
                                            .show();
                                }
                            });
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {
                        hideProgressDialog();
                        try {
                            if (error.networkResponse.statusCode != 0) {
                                Toast.makeText(getContext(),
                                        getString(R.string.mensagem_erro_response) + error.networkResponse.statusCode,
                                        Toast.LENGTH_LONG)
                                        .show();
                            } else {
                                Toast.makeText(getContext(),
                                        getString(R.string.mensagem_erro_get) + error.getMessage(),
                                        Toast.LENGTH_LONG)
                                        .show();
                            }
                        } catch(Exception e){
                            Toast.makeText(getContext(),
                                    getString(R.string.exception_mensagem) + e.getMessage() + " " + error.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                });
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void solicitarPonto(final VolleyCallback callback){
        path = new PathClass(getContext());
        try {
            String nome = nomePonto.getText().toString();
            String endereco = enderecoPonto.getText().toString();
            String descricao = descricaoPonto.getText().toString();
            String telefone = telefonePonto.getText().toString();
            String email = emailPonto.getText().toString();
            //String horarioAtendimento = horarioAtendimentoPonto.getText().toString();
            String msgRequisicao = msgPonto.getText().toString();
            ArrayList<Residuo> residuos = new ArrayList<Residuo>();

            PontoColeta pontoColeta = new PontoColeta(nome, "", endereco, descricao, telefone, email,
                    new HoraAtend(new ArrayList<DiaSemana>()), "0", "0", residuos, msgRequisicao, true);
            session = new SessionManager(getContext());
            session.checkLogin();
            url = path.getServerPath() + "/pontocoleta";

            request = new com.example.samsung.tcc.classes.Request(gson.toJson(pontoColeta), emailU, token);
            requestObj = new JSONObject(gson.toJson(request));

            CustomJSONObjectRequest solicitarPontoReq = new CustomJSONObjectRequest(Request.Method.POST , url, requestObj, getContext(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            callback.onSuccess(response);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            callback.onError(error);
                        }
                    });
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(solicitarPontoReq);
        } catch (IOException e) {
            Log.i("solicitarIONException", e.getMessage() + " causa: " + e.getCause());
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.i("solicitarJSONException", e.getMessage() + " causa: " + e.getCause());
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("solicitarException", e.getMessage() + " causa: " + e.getCause());
        }
    }

    public void hideProgressDialog(){
        if(pDialog.isShowing()){
            pDialog.dismiss();
        }
    }
}
