package com.example.samsung.tcc.classes;

/**
 * Created by Rebeca de Melo on 18/03/2017.
 */
public class Usuario {
    private int codU;
    private String nome;
    private String datanasc;
    private String email;
    private String senha;

    public Usuario(String nome, String datanasc, String email, String senha) {
        super();
        this.nome = nome;
        this.datanasc = datanasc;
        this.email = email;
        this.senha = senha;
    }

    public Usuario(int codU, String nome, String datanasc, String email) {
        this.codU = codU;
        this.nome = nome;
        this.datanasc = datanasc;
        this.email = email;
    }

    public Usuario(int codU, String nome, String datanasc, String email, String senha){
        this.codU = codU;
        this.nome = nome;
        this.datanasc = datanasc;
        this.email = email;
        this.senha = senha;
    }

    public Usuario(String email, String senha){
        this.email = email;
        this.senha = senha;
    }

    public Usuario() {
    }

    public int getCodU() {
        return codU;
    }

    public void setCodU(int codU) {
        this.codU = codU;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDatanasc() {
        return datanasc;
    }

    public void setDatanasc(String datanasc) {
        this.datanasc = datanasc;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
