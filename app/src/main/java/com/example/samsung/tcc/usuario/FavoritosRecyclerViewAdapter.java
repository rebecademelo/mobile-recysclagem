package com.example.samsung.tcc.usuario;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.samsung.tcc.R;
import com.example.samsung.tcc.classes.Residuo;

import java.util.ArrayList;

/**
 * Created by Rebeca de Melo on 02/06/2017.
 */
public class FavoritosRecyclerViewAdapter extends RecyclerView.Adapter<FavoritosRecyclerViewAdapter.FavViewHolder> {
    private Context context;
    private Residuo favResiduoItem;
    private ArrayList<Residuo> favResiduoList = new ArrayList<>();
    private ArrayList<Residuo> favSelectedResiduo = new ArrayList<>();

    public FavoritosRecyclerViewAdapter(Context context, ArrayList<Residuo> favResiduoList) {
        this.context = context;
        this.favResiduoList = favResiduoList;
        this.favSelectedResiduo = new ArrayList<Residuo>();
    }

    @Override
    public int getItemCount() {
        return favResiduoList.size();
    }

    @Override
    public FavViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.favoritos_items, parent, false);
        FavViewHolder holder = new FavViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(FavViewHolder holder, int position) {
        favResiduoItem = favResiduoList.get(position);
        holder.favResiduo.setText(favResiduoItem.getNome());
        holder.favCard.setCardBackgroundColor(Color.parseColor(favResiduoItem.getCategoria().getCor()));
    }

    class FavViewHolder extends RecyclerView.ViewHolder {
        private TextView favResiduo;
        private CheckBox favCheckBox;
        private CardView favCard;
        private int position;

        public FavViewHolder(View view){
            super(view);
            favResiduo = (TextView) view.findViewById(R.id.residuoFav);
            favCheckBox = (CheckBox) view.findViewById(R.id.favCheckBox);
            favCard = (CardView) view.findViewById(R.id.favoritosCard);

            favCheckBox.setOnCheckedChangeListener(null);
            favCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    // posição do card no adapter
                    position = getAdapterPosition();
                    favResiduoItem = favResiduoList.get(position);
                    if(isChecked){
                        favResiduoItem.setSelected(true);
                        favSelectedResiduo.add(favResiduoItem);
                    }else {
                        favResiduoItem.setSelected(false);
                        favSelectedResiduo.remove(favResiduoItem);
                    }
                    favCheckBox.setChecked(favResiduoItem.isSelected());
                }
            });
        }
    }

    public ArrayList<Residuo> getResiduosSelecionados(){
        return this.favSelectedResiduo;
    }
}
