package com.example.samsung.tcc.dica;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.samsung.tcc.classes.VolleyCallback;
import com.example.samsung.tcc.utilities.CustomJsonArrayRequest;
import com.example.samsung.tcc.R;
import com.example.samsung.tcc.classes.Dica;
import com.example.samsung.tcc.classes.Residuo;
import com.example.samsung.tcc.classes.SessionManager;
import com.example.samsung.tcc.drawer.DrawerActivity;
import com.example.samsung.tcc.utilities.PathClass;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class TipoDicaActivity extends AppCompatActivity {
    private Button descarte;
    private Button reutilizacao;
    private Residuo residuo;
    private ArrayList<Dica> listaDica;
    private Gson gson = new Gson();
    private ProgressDialog pDialog;
    private int i;
    private String html;
    private SessionManager session;
    private int codU;
    private String nomeU;
    private String datanascU;
    private String emailU;
    private String token;
    private com.example.samsung.tcc.classes.Request request;
    private PathClass path;
    private String url = "";
    private JSONObject requestObj;
    private Toolbar toolbar;

    private TextView tvtd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_dica);

        descarte = (Button) findViewById(R.id.btnDescarte);
        reutilizacao = (Button) findViewById(R.id.btnReutilizacao);
        //descarte.setClickable(false);
        tvtd = (TextView) findViewById(R.id.tvtd);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        session = new SessionManager(this);
        session.checkLogin();

        HashMap<String, String> usuarioLogado = session.getInfoUsuario();
        codU = Integer.parseInt(usuarioLogado.get(SessionManager.KEY_CODU));
        nomeU = usuarioLogado.get(SessionManager.KEY_NOME);
        datanascU = usuarioLogado.get(SessionManager.KEY_DATA);
        emailU = usuarioLogado.get(SessionManager.KEY_EMAIL);
        token = usuarioLogado.get(SessionManager.KEY_TOKEN);

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.show();

        Thread dicaThread = new Thread(new Runnable() {
            @Override
            public void run() {
                buscaDica(new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray response) {
                        Type listType = new TypeToken<ArrayList<Dica>>(){}.getType();
                        listaDica = (ArrayList<Dica>) gson.fromJson(response.toString(), listType);

                        hideProgressDialog();
                        //descarte.setEnabled(false);
                        reutilizacao.setEnabled(false);
                        descarte.setEnabled(false);
                        for(i=0; i < listaDica.size(); i++) {
                            //Toast.makeText(TipoDicaActivity.this, " " + listaDica.get(i).getCodD(), Toast.LENGTH_LONG).show();
                            if(listaDica.get(i).isReutilizavel()){
                                reutilizacao.setEnabled(true);
                                reutilizacao.setOnClickListener(setButtonClickListener(
                                        listaDica.get(i)
                                ));
                            }else{
                                descarte.setEnabled(true);
                                descarte.setOnClickListener(setButtonClickListener(
                                        listaDica.get(i)
                                ));
                            }
                        }
                    }

                    @Override
                    public void onSuccess(JSONObject response) {

                    }

                    @Override
                    public void onError(VolleyError error) {
                        final VolleyError volleyError = error;
                        try {
                            if (error.networkResponse.statusCode != 0) {
                                hideProgressDialog();
                                Log.i("statusCode", String.valueOf(error.networkResponse.statusCode));
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(TipoDicaActivity.this,
                                                "Ocorreu um problema: " + volleyError.networkResponse.statusCode,
                                                Toast.LENGTH_LONG)
                                                .show();
                                    }
                                });
                            } else if (error instanceof NetworkError) {
                                hideProgressDialog();
                                Log.i("networkError", error.getMessage());
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(TipoDicaActivity.this,
                                                "Network Error: " + volleyError.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    }

                                });
                            } else if (error instanceof ServerError) {
                                hideProgressDialog();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(TipoDicaActivity.this,
                                                "Server Error: " + volleyError.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    }

                                });
                            } else if (error instanceof AuthFailureError) {
                                Log.i("authError", error.getMessage());
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(TipoDicaActivity.this,
                                                "AuthFailure Error: " + volleyError.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    }

                                });
                            } else if (error instanceof ParseError) {
                                hideProgressDialog();
                                Log.i("parseError", error.getMessage());
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(TipoDicaActivity.this,
                                                "Parse Error: " + volleyError.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    }

                                });
                            } else if (error instanceof NoConnectionError) {
                                hideProgressDialog();
                                Log.i("noconnectionError", error.getMessage());
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(TipoDicaActivity.this,
                                                "NoConnection Error: " + volleyError.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    }

                                });
                            } else if (error instanceof TimeoutError) {
                                hideProgressDialog();
                                Log.i("timeoutError", error.getMessage());
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(TipoDicaActivity.this,
                                                "Timeout Error: " + volleyError.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    }

                                });
                            } else {
                                Log.i("else", error.getMessage());
                                hideProgressDialog();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(TipoDicaActivity.this,
                                                "Ocorreu um problema: " + volleyError.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    }

                                });
                        /*Toast.makeText(getContext(),
                                error.getMessage(),
                                Toast.LENGTH_LONG)
                                .show();*/
                            }
                        } catch (final Exception e) {
                            Log.i("exececao", error.getMessage() + " & " + e.getMessage());
                            hideProgressDialog();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(TipoDicaActivity.this,
                                            "Ocorreu uma exceção: " + e.getMessage(),
                                            Toast.LENGTH_LONG)
                                            .show();
                                }

                            });
                        }
                    }
                });
            }
        });

        if(getIntent().hasExtra("lista_dica")) { // em caso de estar voltando de uma intent de dica
            String listaDicaJson = getIntent().getStringExtra("lista_dica");
            Type listType = new TypeToken<ArrayList<Dica>>(){}.getType();
            listaDica = gson.fromJson(listaDicaJson, listType);
            reutilizacao.setEnabled(false);
            descarte.setEnabled(false);
            for(i=0; i < listaDica.size(); i++) {
                if(listaDica.get(i).isReutilizavel()){
                    reutilizacao.setEnabled(true);
                    reutilizacao.setOnClickListener(setButtonClickListener(
                            listaDica.get(i)
                    ));
                }else{
                    descarte.setEnabled(true);
                    descarte.setOnClickListener(setButtonClickListener(
                            listaDica.get(i)
                    ));
                }
            }
            pDialog.hide();
        } else {
            dicaThread.run();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Intent intent = new Intent(this, DrawerActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                onBackPressed();
                break;
        }
        return true;
    }

    private void buscaDica(final VolleyCallback callback){

        residuo = new Residuo();
        residuo = (Residuo) getIntent().getSerializableExtra("residuo");

        path = new PathClass(getApplicationContext());
        try {
            url = path.getServerPath() + "/dica/doresiduo";
            //Toast.makeText(TipoDicaActivity.this, url, Toast.LENGTH_LONG).show();
            request = new com.example.samsung.tcc.classes.Request(gson.toJson(residuo), emailU, token);
            requestObj = new JSONObject(gson.toJson(request));
            CustomJsonArrayRequest stringRequest = new CustomJsonArrayRequest(Request.Method.POST , url, requestObj, getBaseContext(),
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            callback.onSuccess(response);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            callback.onError(error);
                        }
                    });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(TipoDicaActivity.this, R.string.json_erro_criar + " " + e, Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(TipoDicaActivity.this, R.string.properties_erro_caminho + " " + e, Toast.LENGTH_LONG).show();
        }
    }

    private View.OnClickListener setButtonClickListener(final Dica dica) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("dica", gson.toJson(dica));
                bundle.putString("lista_dica", gson.toJson(listaDica));
                //String html = listaDica.get(i).getDescricaoPath();
                Intent intent = new Intent(TipoDicaActivity.this, DicaDescarteActivity.class);
                //intent.putExtra("html", html);
                intent.putExtras(bundle);
                startActivity(intent);
                //finish();
                //Toast.makeText(TipoDicaActivity.this, " " + gson.toJson(dica), Toast.LENGTH_LONG).show();
            }
        };
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void hideProgressDialog(){
        if(pDialog.isShowing()){
            pDialog.dismiss();
        }
    }
}
