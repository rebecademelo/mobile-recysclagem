package com.example.samsung.tcc.pontocoleta;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.samsung.tcc.R;
import com.example.samsung.tcc.classes.PontoColeta;
import com.example.samsung.tcc.classes.Residuo;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.util.HashMap;

/**
 * Created by Rebeca de Melo on 16/06/2017.
 */
public class PontoInfoWindow implements GoogleMap.InfoWindowAdapter {
    private Context context;
    private HashMap<Marker, PontoColeta> pinInfo = new HashMap<>();
    private View view;
    // info do balão do pin
    private TextView nome;
    private TextView endereco;
    private TextView horaAtendimento;
    private TextView telefone;
    private TextView email;
    private TextView residuos;

    public PontoInfoWindow(Context context, HashMap<Marker, PontoColeta> pinInfo) {
        this.context = context;
        this.pinInfo = pinInfo;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        PontoColeta pc = pinInfo.get(marker);
        if(pc != null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.marker_info_content, null);

            nome = (TextView) view.findViewById(R.id.nomePonto);
            endereco = (TextView) view.findViewById(R.id.enderecoPonto);
            horaAtendimento = (TextView) view.findViewById(R.id.horaAtendimentoPonto);
            telefone = (TextView) view.findViewById(R.id.telefonePonto);
            email = (TextView) view.findViewById(R.id.emailPonto);
            residuos = (TextView) view.findViewById(R.id.residuosPonto);
            // info que irao aparecer no balao sobre o ponto de coleta
            nome.setText(pc.getNome());
            endereco.setText(pc.getEndereco());
            horaAtendimento.setText(pc.getHoraAtendimentoBD());
            telefone.setText(pc.getTelefone());
            email.setText(pc.getEmail());
            String concText = "";
            int i = 0;
            for (Residuo res : pc.getResiduos()){
                concText = concText + res.getNome();
                i++;
                if(i == pc.getResiduos().size()){
                    break;
                }
                concText = concText + "\n";
            }
            residuos.setText(concText);
            Log.i("pinInfo", pc.getNome());
        }else {
            Log.i("pinInfoError", "O ponto está sem suas informações.");
        }
        return view;
    }
}
