package com.example.samsung.tcc.classes;

import com.example.samsung.tcc.classes.subcls.DiaSemana;
import com.example.samsung.tcc.classes.subcls.HoraAtend;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * Created by Rebeca de Melo on 04/05/2017.
 */
public class PontoColeta {
    private int codPC;
    private String nome;
    private String pathImagem;
    private String endereco;
    private String descricao;
    private String telefone;
    private String email;
    private HoraAtend horaAtendimento;
    //private String horaAtendimento;
    private double latitude;
    private double longitude;
    private String msgRequisicao;
    private boolean pendente = false;
    private List<Residuo> residuos;

    public PontoColeta(int codPC, String nome, String imagem, String endereco, String descricao, String telefone, String email,
                       String horaAtendimento, String latitudeBD, String longitudeBD, List<Residuo> residuos, String msgRequisicao,
                       boolean pendente) throws Exception {
        this(nome, imagem, endereco, descricao, telefone, email, horaAtendimento, latitudeBD, longitudeBD, residuos, msgRequisicao, pendente);
        this.codPC = codPC;
    }

    public PontoColeta(String nome, String imagem, String endereco, String descricao, String telefone, String email,
                       String horaAtendimento, String latitudeBD, String longitudeBD, List<Residuo> residuos, String msgRequisicao,
                       boolean pendente) throws Exception {
        super();
        //this.codPC = codPC;
        this.nome = nome;
        this.pathImagem = imagem;
        this.endereco = endereco;
        this.descricao = descricao;
        this.telefone = telefone;
        this.email = email;
        setHoraAtendimento(horaAtendimento);
        //this.horaAtendimento = horaAtendimento;
        setCoordenada(latitudeBD, longitudeBD);
        this.residuos = residuos;
        this.msgRequisicao = msgRequisicao;
        this.pendente = pendente;
    }

    public PontoColeta(String nome, String imagem, String endereco, String descricao, String telefone, String email,
                       HoraAtend horaAtendimento, String latitudeBD, String longitudeBD, List<Residuo> residuos, String msgRequisicao,
                       boolean pendente) throws Exception {
        super();
        this.nome = nome;
        this.pathImagem = imagem;
        this.endereco = endereco;
        this.descricao = descricao;
        this.telefone = telefone;
        this.email = email;
        this.horaAtendimento = horaAtendimento;
        setCoordenada(latitudeBD, longitudeBD);
        this.residuos = residuos;
        this.msgRequisicao = msgRequisicao;
        this.pendente = pendente;
    }

    public int getCodPC() {
        return codPC;
    }

    public void setCodPC(int codPC) {
        this.codPC = codPC;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getImagem() {
        return pathImagem;
    }

    public void setImagem(String imagem) {
        this.pathImagem = imagem;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public HoraAtend getHoraAtendimento() {
        return horaAtendimento;
    }

    public void setHoraAtendimento(HoraAtend horaAtendimento) {
        this.horaAtendimento = horaAtendimento;
    }

    public String getHoraAtendimentoStr() {
        return horaAtendimento.toString();
    }

    /**
     * Esqueça strings
     * Retorna algo como: "2,09:00,16:00 7,09:00,13:00"
     */
    public String getHoraAtendimentoBD() {

        Collections.sort(this.horaAtendimento.getDiasSemana(), new Comparator<DiaSemana>(){
            public int compare(DiaSemana o1, DiaSemana o2) {
                return o1.getDia() - o2.getDia();
            }
        });

        StringBuilder sb = new StringBuilder();
        int count = 0;
        for(DiaSemana ds : this.horaAtendimento.getDiasSemana()){
            count++;
            sb.append(ds.getDia()+",");
            sb.append(ds.getAbertura()+",");
            sb.append(ds.getEncerramento());
            if( !(count == this.horaAtendimento.getDiasSemana().size()) ){
                sb.append(" ");
            }
        }
        return sb.toString();
    }
    /**
     * String horaAtentimento deve ser passado como: "2,09:00,16:00 7,09:00,13:00"
     * @param horaAtendimento
     */
    public void setHoraAtendimento(String horaAtendimento) {
        try{
            if(horaAtendimento != null && !horaAtendimento.isEmpty()) {
                String[] dias = {horaAtendimento};
                if(horaAtendimento.contains(" ")) {
                    dias = horaAtendimento.split(" ");
                }
                List<DiaSemana> listDias = new ArrayList<DiaSemana>();

                for(String diaSemanaHora : dias){
                    String[] diaOuHora = diaSemanaHora.split(",");
                    //De segunda à sexta, por exemplo: 2-6
                    if(diaOuHora.length == 3){
                        listDias.add(new DiaSemana(
                                Integer.valueOf(diaOuHora[0]),
                                diaOuHora[1],
                                diaOuHora[2]
                        ));
                    } else {
                        throw new Exception();
                    }
                }

                this.horaAtendimento = new HoraAtend(listDias);
            }
        } catch(Exception e){
            System.err.println("Erro ao setar horários de atendimento: " + horaAtendimento);
        }
    }

    public Double getLatitude() {
        return Double.valueOf(this.latitude);
    }

    public Double getLongitude() {
        return Double.valueOf(this.longitude);
    }

    public String getCoordenadaStr() {
        return (latitude + ", " + longitude);
    }

    /**
     * Formatos de coordenada:
     * 1) decimal: 		-25.456732, -49.236001
     * 2) hexa (h/m/s): 25°27'24.2"S 49°14'09.6"W
     * @param latitudeBD
     * @param longitudeBD
     * @throws Exception
     */
    public void setCoordenada(String latitudeBD, String longitudeBD) throws Exception {
        //não queremos usar esse aqui
        if(isCoordHexaFormat(latitudeBD)){
            latitudeBD = latitudeBD.replace("º", "-")
                    .replace("\"", "-")
                    .replace("\'", "-");
            longitudeBD = longitudeBD.replace("º", "-")
                    .replace("\"", "-")
                    .replace("\'", "-");
            String[] latitude = latitudeBD.split("-");
            String[] longitude = latitudeBD.split("-");
            if(latitude.length == 4 && longitude.length == 4
                    && latitude[1].length() == 1 && longitude[1].length() == 1)
            {
                //TODO
            } else {
                throw new Exception("Formato incorreto de coordenadas! Deveria ser algo como -1234 e +1234', é "
                        + latitudeBD + " e " + longitudeBD);
            }
            //decimal - esse é maneiro
        } else {
            try {
                NumberFormat format = NumberFormat.getInstance(Locale.FRENCH);
                Number number = format.parse("1,234");
                double d = number.doubleValue();
                this.latitude = Double.valueOf(latitudeBD);
                this.longitude = Double.valueOf(longitudeBD);
            } catch (Exception e) {
                throw new Exception("Formato incorreto de coordenadas! Deveria ser algo como -24.1234 e +25.1234, é "
                        + latitudeBD + " e " + longitudeBD);
            }
        }
    }

    public boolean isCoordHexaFormat(String coordinate) {
        if(
            coordinate.toUpperCase().contains("N") ||
            coordinate.toUpperCase().contains("S") ||
            coordinate.toUpperCase().contains("W") ||
            coordinate.toUpperCase().contains("E") ) {
            return true;
        }
        return false;
    }

    public List<Residuo> getResiduos() {
        return residuos;
    }

    public void setResiduos(List<Residuo> residuos) {
        this.residuos = residuos;
    }

    @Override
    public boolean equals(Object object) {
        boolean sameSame = false;

        if (object != null && object instanceof PontoColeta) {
            sameSame = (this.codPC == ((PontoColeta) object).codPC);
        }

        return sameSame;
    }


    public String getMsgRequisicao() {
        if (msgRequisicao == null) return "";
        return msgRequisicao;
    }

    public void setMsgRequisicao(String msgRequisicao) {
        this.msgRequisicao = msgRequisicao;
    }

    public boolean isPendente() {
        return pendente;
    }

    public void setPendente(boolean pendente) {
        this.pendente = pendente;
    }

}
