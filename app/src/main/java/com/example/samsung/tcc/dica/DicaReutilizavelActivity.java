package com.example.samsung.tcc.dica;

import android.content.Intent;
import android.os.NetworkOnMainThreadException;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.example.samsung.tcc.R;
import com.example.samsung.tcc.classes.Dica;
import com.example.samsung.tcc.utilities.PropertySource;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class DicaReutilizavelActivity extends AppCompatActivity {
    private WebView dicaReutilizavel;
    private Dica dica;
    private ArrayList<Dica> listaDica;
    private String dicaUrl;
    private String dicaUrlContent;
    private Toolbar toolbar;
    private Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dica_reutilizavel);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        dicaReutilizavel = (WebView) findViewById(R.id.dicaReutilizavel);
        WebSettings settings = dicaReutilizavel.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        dicaReutilizavel.setHorizontalScrollBarEnabled(true);
        dicaReutilizavel.setVerticalScrollBarEnabled(true);

        // pegando extras da intent anterior
        Intent intent = getIntent();
        String dicaJson = intent.getExtras().getString("dica"); //"http://10.0.3.2:8008/file/dica/1-r.html";
        dica = gson.fromJson(dicaJson, Dica.class);
        String listaDicaJson = intent.getExtras().getString("lista_dica");
        Type listType = new TypeToken<ArrayList<Dica>>(){}.getType();
        listaDica = gson.fromJson(listaDicaJson, listType);

        String url = dica.getDescricaoPath();
        try {
            if (!url.startsWith("http://")) {
                String fixedUrl = "http://" + url;
                url = fixedUrl;
            }
            String fixedUrl = fixLocalHost(url);
            dicaUrl = fixedUrl;

            fixContentURLs();

            for(int i = 0; i < 10; i++){
                if(dicaUrlContent != null){
                    dicaReutilizavel.loadData(dicaUrlContent, "text/html; charset=utf-8", "UTF-8");
                    //Toast.makeText(DicaReutilizavelActivity.this, " " + dicaUrlContent, Toast.LENGTH_LONG).show();
                    break;
                } else {
                    //wait
                    try {
                        Toast.makeText(DicaReutilizavelActivity.this, " " + R.string.aguardando_carregar, Toast.LENGTH_SHORT).show();
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        }catch(NetworkOnMainThreadException nomt) {
            nomt.printStackTrace();
            dicaReutilizavel.loadData("<p>Erro de rede: " + nomt.getMessage(), "text/html", "UTF-8");
        } catch(RuntimeException rte){
            rte.printStackTrace();
            dicaReutilizavel.loadData("<p>Runtime error: " + rte.getMessage(), "text/html", "UTF-8");
        } catch (IOException ioe) {
            ioe.printStackTrace();
            dicaReutilizavel.loadData("<p>IO error: " + ioe.getMessage(), "text/html", "UTF-8");
        }
    }

    private void fixContentURLs() {

        DicaReutilizavelActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    StringBuilder birl = new StringBuilder();

                    URL origin = null;

                    origin = new URL(dicaUrl);

                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(
                                    origin.openStream()
                            ));
                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {
                        birl.append(inputLine);
                    }
                    in.close();

                    dicaUrlContent = fixLocalHost(birl.toString());

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    dicaReutilizavel.loadData("<p>URL error: " + e.getMessage(), "text/html", "UTF-8");
                } catch (IOException e) {
                    e.printStackTrace();
                    dicaReutilizavel.loadData("<p>IO error: " + e.getMessage(), "text/html", "UTF-8");
                }
            }
        });
    }

    private String fixLocalHost(String str) throws IOException {
        String myLocalhost = PropertySource.getProperty("resysclagem.service.host", getApplicationContext());
        return str.replaceAll("localhost", myLocalhost);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Intent intent = new Intent(this, TipoDicaActivity.class); // discutir se isso precisa ser mudado
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        Bundle bundle = new Bundle();
        bundle.putString("lista_dica", gson.toJson(listaDica));
        Intent backIntent = new Intent(this, TipoDicaActivity.class);
        backIntent.putExtras(bundle);

        startActivity(backIntent);
        super.onBackPressed();

        finish();
    }

}
