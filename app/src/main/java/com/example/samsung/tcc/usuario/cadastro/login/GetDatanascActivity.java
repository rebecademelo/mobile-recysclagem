package com.example.samsung.tcc.usuario.cadastro.login;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.samsung.tcc.R;
import com.example.samsung.tcc.classes.ContaUsuario;
import com.example.samsung.tcc.classes.MsgResponse;
import com.example.samsung.tcc.classes.Request;
import com.example.samsung.tcc.classes.SessionManager;
import com.example.samsung.tcc.classes.Usuario;
import com.example.samsung.tcc.drawer.DrawerActivity;
import com.example.samsung.tcc.utilities.CustomJSONObjectRequest;
import com.example.samsung.tcc.utilities.PathClass;
import com.example.samsung.tcc.utilities.SHAHelper;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class GetDatanascActivity extends AppCompatActivity {
    private EditText datanascG;
    private Button dataNlogin;
    private Gson gson = new Gson();
    private Usuario usuario;
    private String id;
    private String nome;
    private String email;
    private String datanasc;
    private String senha;
    private SessionManager session;
    //request
    private com.example.samsung.tcc.classes.Request request;
    private PathClass path;
    private String url = "";
    private JSONObject requestObj;
    private JSONObject jsonObject;
    private Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_datanasc);

        session = new SessionManager(getApplicationContext());

        calendar = Calendar.getInstance();

        datanascG = (EditText) findViewById(R.id.datanascG);
        dataNlogin = (Button) findViewById(R.id.dataNLogin);

        /*datanascG.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(GetDatanascActivity.this, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });*/

        datanascG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(GetDatanascActivity.this, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });

        dataNlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                path = new PathClass(getApplicationContext());
                try {
                    url = path.getServerPath() + "/usuario/cadastro";
                    Intent i = getIntent();
                    id = i.getExtras().getString("id");
                    nome = i.getExtras().getString("nome");
                    datanasc = datanascG.getText().toString();
                    email = i.getExtras().getString("email");
                    senha = "g" + id;

                    SHAHelper shaHelper = new SHAHelper();
                    String senhaG = shaHelper.getSHA256(senha);

                    usuario = new Usuario(nome, datanasc, email, senhaG);

                    jsonObject = new JSONObject(gson.toJson(usuario));
                    CustomJSONObjectRequest stringRequest = new CustomJSONObjectRequest(url, jsonObject, getBaseContext(),
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                MsgResponse msgResponse = gson.fromJson(response.toString(), MsgResponse.class);
                                if(msgResponse.isStatus()){
                                    Usuario usuarioCadastrado = gson.fromJson(msgResponse.getExtra(), Usuario.class);
                                    session.loginSession(usuarioCadastrado.getCodU(),nome, datanasc, email, "token");
                                    goToListaResiduos();
                                } else {
                                    Toast.makeText(GetDatanascActivity.this, msgResponse.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                try {
                                    if (error.networkResponse.statusCode != 0) {
                                        Toast.makeText(GetDatanascActivity.this, R.string.mensagem_erro_response + " "  + error.networkResponse.statusCode, Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(GetDatanascActivity.this, R.string.mensagem_erro_get + " " + error.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                } catch(Exception e){
                                    Toast.makeText(GetDatanascActivity.this, R.string.exception_mensagem + " " + error.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(stringRequest);
                }catch (JSONException e){
                    e.printStackTrace();
                    Toast.makeText(GetDatanascActivity.this, R.string.facebook_erro_busca, Toast.LENGTH_LONG).show(); // 2, 5
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }
    };

    private void updateLabel() {
        String dateFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

        datanascG.setText(sdf.format(calendar.getTime()));
    }

    private void goToListaResiduos(){
        Intent intent = new Intent(GetDatanascActivity.this, DrawerActivity.class);
        startActivity(intent);
    }
}
