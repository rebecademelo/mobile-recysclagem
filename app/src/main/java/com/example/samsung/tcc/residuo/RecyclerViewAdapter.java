package com.example.samsung.tcc.residuo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.samsung.tcc.classes.MsgResponse;
import com.example.samsung.tcc.classes.Residuo;
import com.example.samsung.tcc.R;
import com.example.samsung.tcc.classes.SessionManager;
import com.example.samsung.tcc.classes.Usuario;
import com.example.samsung.tcc.classes.UsuarioResiduo;
import com.example.samsung.tcc.classes.VolleyCallback;
import com.example.samsung.tcc.dica.TipoDicaActivity;
import com.example.samsung.tcc.utilities.CheckHost;
import com.example.samsung.tcc.utilities.ColorUtil;
import com.example.samsung.tcc.utilities.CustomJSONObjectRequest;
import com.example.samsung.tcc.utilities.PathClass;
import com.google.gson.Gson;
import com.silencedut.expandablelayout.ExpandableLayout;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Rebeca de Melo on 22/03/2017.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.CustomViewHolder>{
    Context context;
    static boolean toast = false;
    private Gson gson = new Gson();
    private HashSet<Integer> expandedPosition;
    private ArrayList<Residuo> residuoList;
    private ArrayList<Residuo> residuoFavoritoList = new ArrayList<>();
    private ArrayList<Boolean> isFavoritoSelecionado = new ArrayList<>();
    private ArrayList<Residuo> filterResiduo;
    private ArrayList<Residuo> selectedResiduo = new ArrayList<>();
    private Usuario usuario;
    private PathClass path;
    private String url = "";
    private JSONObject requestObj;
    private com.example.samsung.tcc.classes.Request request;
    private SessionManager session;
    private String token;
    private UsuarioResiduo usuarioResiduo;
    private Residuo residuoItem;
    private CheckHost host = new CheckHost();

    public RecyclerViewAdapter() {
    }

    public RecyclerViewAdapter(Context context, ArrayList<Residuo> residuoList, ArrayList<Residuo> residuoFavoritoList, Usuario usuario) { // nomeResiduo = filterList & residuoList = listItems
        this.context = context;
        this.residuoList = residuoList;
        for(int i = 0; i < residuoList.size(); i++){
            isFavoritoSelecionado.add(false);
        }
        this.usuario = usuario;
        expandedPosition = new HashSet<>();
        this.filterResiduo = new ArrayList<Residuo>();//residuoList;
        this.filterResiduo.addAll(this.residuoList);
        this.selectedResiduo = new ArrayList<Residuo>();
        this.residuoFavoritoList = residuoFavoritoList;
        Picasso.Builder builder = new Picasso.Builder(getApplicationContext());
        builder.listener(new Picasso.Listener() {
            @Override
            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                exception.printStackTrace();
            }
        });
    }

    @Override
    public int getItemCount() {
        //return residuoList.size();
        return (null != filterResiduo ? filterResiduo.size() : 0);
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_items, parent, false);
        CustomViewHolder holder = new CustomViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int i) {
        holder.updateItem(i); // para expandir o card
        residuoItem = residuoList.get(i);
        holder.nomeResiduo.setText(residuoItem.getNome());
        holder.descricaoResiduo.setText(residuoItem.getDescricao());
        String cor = residuoItem.getCategoria().getCor();
        holder.card.setCardBackgroundColor(Color.parseColor(cor));
        holder.nomeResiduo.setTextColor(
                ColorUtil.isDark(Color.parseColor(cor)) ? Color.WHITE : Color.BLACK
                );
        holder.descricaoResiduo.setTextColor(
                ColorUtil.isDark(Color.parseColor(cor)) ? Color.WHITE : Color.BLACK
        );
        // String imgPath = residuoItem.getImagemResiduo().replace("-big", "-mini");
        try {
            residuoItem.fixImagemLocalhost(context);
            Picasso.with(context).load(residuoItem.getImagemResiduo())
                    //.resize(70, 70)
                    .fit().centerCrop()
                    .into(holder.img_residuo);
            Log.i("picasso", residuoItem.getImagemResiduo());
        } catch (Exception e) {
            Log.e("picasso", residuoItem.getImagemResiduo());
            e.printStackTrace();
        }
        Log.i("favRV", String.valueOf(residuoFavoritoList.size()));
        holder.textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down_white_24px, 0);
        holder.textView.setText("Mostrar mais");
        holder.textView.setTextColor(
                ColorUtil.isDark(Color.parseColor(cor)) ? Color.WHITE : Color.BLACK
        );
    }

    public ArrayList<Residuo> getResiduosSelecionados(){
        return this.selectedResiduo;
    }

    public void filtraBusca(final String txt){
        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean run = true;
                toast = false;
                if(txt.length() == 0){
                    filterResiduo = residuoList;
                } else if (txt.length() < 0){

                } else {
                    filterResiduo = new ArrayList<Residuo>();
                    for(Residuo residuoItem : residuoList){
                        if(residuoItem.getNome().toLowerCase().contains(txt.toLowerCase())){
                            filterResiduo.add(residuoItem);
                        }
                    }
                    if (filterResiduo.size() == 0){
                        toast = true;
                    }
                }
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(toast)
                            Toast.makeText(context, R.string.residuo_inexistente, Toast.LENGTH_LONG).show();
                        // Notify the List that the DataSet has changed...
                        notifyDataSetChanged();
                    }
                });
                run = false;
                if(!run){
                    return;
                }
            }
        }).start();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        private ExpandableLayout expandableLayout;
        private CheckBox checkBox;
        private TextView nomeResiduo;
        private TextView descricaoResiduo;
        private ImageView img_residuo;
        private Button dica;
        private Button favorito;
        private CardView card;
        // textview para mostrar mais coisas
        public TextView textView;
        private int position;

        public CustomViewHolder(View v) {
            super(v);
            expandableLayout = (ExpandableLayout) v.findViewById(R.id.expandable_layout);
            checkBox = (CheckBox) v.findViewById(R.id.checkBox);
            nomeResiduo = (TextView) v.findViewById(R.id.nomeResiduoTV);
            descricaoResiduo = (TextView) v.findViewById(R.id.descricaoResiduoTV);
            img_residuo = (ImageView) v.findViewById(R.id.img_residuoIV);
            dica = (Button) v.findViewById(R.id.btnDica);
            favorito = (Button) v.findViewById(R.id.btnFavorito);
            card = (CardView) v.findViewById(R.id.card_view);
            textView = (TextView) v.findViewById(R.id.vermais_menosTV);

            checkBox.setOnCheckedChangeListener(null);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    position = getAdapterPosition();
                    residuoItem = residuoList.get(position);
                    Log.i("position", String.valueOf(position));
                    if(isChecked){
                        residuoItem.setSelected(true);
                        selectedResiduo.add(residuoItem);
                        Log.i("selectedResiduo", residuoItem.getNome() + " " + selectedResiduo.size());
                    }else {
                        residuoItem.setSelected(false);
                        selectedResiduo.remove(residuoItem);
                        Log.i("deselectedResiduo", residuoItem.getNome() + " " + selectedResiduo.size());
                    }
                    checkBox.setChecked(residuoItem.isSelected());
                }
            });

            favorito.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    position = getAdapterPosition();
                    residuoItem = residuoList.get(position);
                    session = new SessionManager(context);
                    session.checkLogin();

                    HashMap<String, String> usuarioLogado = session.getInfoUsuario();
                    token = usuarioLogado.get(SessionManager.KEY_TOKEN);

                    path = new PathClass(context);
                    usuarioResiduo = new UsuarioResiduo(usuario, residuoItem);
                    request = new com.example.samsung.tcc.classes.Request(gson.toJson(usuarioResiduo), usuario.getEmail(), token);
                    if(!isFavoritoSelecionado.get(position)){
                        favoritarResiduo(new VolleyCallback() {
                            @Override
                            public void onSuccess(JSONArray response) {

                            }

                            @Override
                            public void onSuccess(JSONObject response) {
                                MsgResponse msgResponse = gson.fromJson(response.toString(), MsgResponse.class);
                                if(msgResponse.isStatus()){
                                    Log.i("holderPosition", position + " " + residuoItem.getNome());
                                    favorito.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bookmark_black_24px, 0, 0, 0);
                                    isFavoritoSelecionado.set(position, true);
                                    residuoFavoritoList.add(residuoItem);
                                    Log.i("favoritar", String.valueOf(msgResponse.isStatus() + ": " + msgResponse.getMessage()));
                                }
                            }

                            @Override
                            public void onError(VolleyError error) {
                                try {
                                    if (error.networkResponse.statusCode != 0) {
                                        Log.i("statusCode", String.valueOf(error.networkResponse.statusCode) + " " + error.getMessage());
                                        /*Toast.makeText(context,
                                                "Ocorreu um problema: " + error.networkResponse.statusCode,
                                                Toast.LENGTH_LONG)
                                                .show();*/
                                    } else if (error instanceof NetworkError) {
                                        Log.i("networkError", error.getMessage());
                                        Toast.makeText(context,
                                                "Network Error: " + error.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    } else if (error instanceof ServerError) {
                                        Toast.makeText(context,
                                                "Server Error: " + error.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    } else if (error instanceof AuthFailureError) {
                                        Log.i("authError", error.getMessage());
                                        Toast.makeText(context,
                                                "AuthFailure Error: " + error.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    } else if (error instanceof ParseError) {
                                        Log.i("parseError", error.getMessage());
                                        Toast.makeText(context,
                                                "Parse Error: " + error.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    } else if (error instanceof NoConnectionError) {
                                        Log.i("noconnectionError", error.getMessage());
                                        Toast.makeText(context,
                                                "NoConnection Error: " + error.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    } else if (error instanceof TimeoutError) {
                                        Log.i("timeoutError", error.getMessage());
                                        Toast.makeText(context,
                                                "Timeout Error: " + error.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    } else {
                                        Log.i("else", error.getMessage());
                                        /*Toast.makeText(context,
                                                "Ocorreu um problema: " + error.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();*/
                                    }
                                } catch (Exception e) {
                                    Log.i("exececao", error.getMessage() + " & " + e.getMessage());
                                    /*Toast.makeText(context,
                                            "Ocorreu uma exceção: " + e.getMessage(),
                                            Toast.LENGTH_LONG)
                                            .show();*/
                                }
                            }
                        });
                    } else {
                        desfavoritarResiduo(new VolleyCallback() {
                            @Override
                            public void onSuccess(JSONArray response) {

                            }

                            @Override
                            public void onSuccess(JSONObject response) {
                                MsgResponse msgResponse = gson.fromJson(response.toString(), MsgResponse.class);
                                if(msgResponse.isStatus()){
                                    Log.i("holderPosition", position + " " + residuoItem.getNome());
                                    favorito.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bookmark_border_black_24px, 0, 0, 0);
                                    isFavoritoSelecionado.set(position, false);
                                    residuoFavoritoList.remove(residuoItem);
                                    Log.i("desfavoritar", String.valueOf(msgResponse.isStatus()) + msgResponse.getMessage());
                                }
                            }

                            @Override
                            public void onError(VolleyError error) {
                                try {
                                    if (error.networkResponse.statusCode != 0) {
                                        Log.i("statusCode", String.valueOf(error.networkResponse.statusCode));
                                        /*Toast.makeText(context,
                                                "Ocorreu um problema: " + error.networkResponse.statusCode,
                                                Toast.LENGTH_LONG)
                                                .show();*/
                                    } else if (error instanceof NetworkError) {
                                        Log.i("networkError", error.getMessage());
                                        Toast.makeText(context,
                                                "Network Error: " + error.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    } else if (error instanceof ServerError) {
                                        Toast.makeText(context,
                                                "Server Error: " + error.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    } else if (error instanceof AuthFailureError) {
                                        Log.i("authError", error.getMessage());
                                        Toast.makeText(context,
                                                "AuthFailure Error: " + error.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    } else if (error instanceof ParseError) {
                                        Log.i("parseError", error.getMessage());
                                        Toast.makeText(context,
                                                "Parse Error: " + error.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    } else if (error instanceof NoConnectionError) {
                                        Log.i("noconnectionError", error.getMessage());
                                        Toast.makeText(context,
                                                "NoConnection Error: " + error.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    } else if (error instanceof TimeoutError) {
                                        Log.i("timeoutError", error.getMessage());
                                        Toast.makeText(context,
                                                "Timeout Error: " + error.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    } else {
                                        Log.i("else", error.getMessage());
                                        /*Toast.makeText(context,
                                                "Ocorreu um problema: " + error.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();*/
                                    }
                                } catch (Exception e) {
                                    Log.i("exececao", error.getMessage() + " & " + e.getMessage());
                                    /*Toast.makeText(context,
                                            "Ocorreu uma exceção: " + e.getMessage(),
                                            Toast.LENGTH_LONG)
                                            .show();*/
                                }
                            }
                        });
                    }
                }
            });

            dica.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    position = getAdapterPosition();
                    residuoItem = residuoList.get(position);
                    Log.i("btnDica", residuoItem.getNome());
                    Intent intent = new Intent(context, TipoDicaActivity.class);
                    intent.putExtra("residuo", residuoItem);
                    context.startActivity(intent);
                }
            });
        }

        private void updateItem(int i){
            this.position = i;
            expandableLayout.setOnExpandListener(new ExpandableLayout.OnExpandListener() {
                @Override
                public void onExpand(boolean expanded) {
                    registerExpand(position, textView, dica, favorito);
                }
            });
            expandableLayout.setExpand(expandedPosition.contains(position));
        }

        private void registerExpand(final int position, TextView tv, Button dicaBtn, final Button favBtn){
            if(expandedPosition.contains(position)){
                removeExpand(position);
                tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down_white_24px, 0);
                tv.setText("Mostrar mais");
            } else {
                addExpand(position);
                tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_up_white_24px, 0);
                tv.setText("Mostrar menos");
                if(residuoFavoritoList.contains(residuoList.get(position))){
                    favBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bookmark_black_24px, 0, 0, 0);
                    isFavoritoSelecionado.add(position, true);
                }else {
                    favBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bookmark_border_black_24px, 0, 0, 0);
                    isFavoritoSelecionado.add(position, false);
                }
            }
        }

        private void removeExpand(int position) {
            expandedPosition.remove(position);
        }

        private void addExpand(int position) {
            expandedPosition.add(position);
        }
    }

    private void favoritarResiduo(final VolleyCallback callback){
        path = new PathClass(context);
        try {
            url = path.getServerPath() + "/usuario/favoritar";
            requestObj = new JSONObject(gson.toJson(request));
            CustomJSONObjectRequest favoritarReq = new CustomJSONObjectRequest(Request.Method.POST , url, requestObj, context,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            callback.onSuccess(response);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            callback.onError(error);
                        }
                    });
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(favoritarReq);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void desfavoritarResiduo(final VolleyCallback callback){
        path = new PathClass(context);
        try {
            url = path.getServerPath() + "/usuario/desfavoritar";
            requestObj = new JSONObject(gson.toJson(request));
            CustomJSONObjectRequest desfavoritarReq = new CustomJSONObjectRequest(Request.Method.POST , url, requestObj, context,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            callback.onSuccess(response);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            callback.onError(error);
                        }
                    });
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(desfavoritarReq);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

