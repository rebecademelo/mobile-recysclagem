package com.example.samsung.tcc.classes;

import java.io.Serializable;

/**
 * Created by Rebeca de Melo on 30/04/2017.
 */
public class Categoria implements Serializable{
    private int codC;
    private String nome;
    private String cor;

    public Categoria(int cod, String nome, String cor) {
        this.codC = cod;
        this.nome = nome;
        this.cor = cor;
    }

    public int getCodC() {
        return codC;
    }

    public void setCodC(int cod) {
        this.codC = cod;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }
}