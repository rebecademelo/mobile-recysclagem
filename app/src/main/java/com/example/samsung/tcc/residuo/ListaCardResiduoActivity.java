package com.example.samsung.tcc.residuo;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
//import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.samsung.tcc.utilities.CheckHost;
import com.example.samsung.tcc.utilities.CustomJsonArrayRequest;
import com.example.samsung.tcc.classes.Residuo;
import com.example.samsung.tcc.R;
import com.example.samsung.tcc.classes.SessionManager;
import com.example.samsung.tcc.classes.Usuario;
import com.example.samsung.tcc.classes.VolleyCallback;
import com.example.samsung.tcc.pontocoleta.MapaFragment;
import com.example.samsung.tcc.drawer.DrawerActivity;
import com.example.samsung.tcc.utilities.HttpHandler;
import com.example.samsung.tcc.utilities.PathClass;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;


public class ListaCardResiduoActivity extends Fragment {
    private SessionManager session;
    private int codU;
    private String nomeU;
    private String datanascU;
    private String emailU;
    private Usuario usuario;
    private Gson gson = new Gson();
    private static ProgressDialog pDialog;
    private ProgressDialog pDialogFav;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerViewAdapter adapter = new RecyclerViewAdapter();
    private ArrayList<Residuo> infoResiduos = new ArrayList<>();
    private ArrayList<Residuo> residuoFavorito = new ArrayList<>();
    private PathClass path;
    private String url = "";
    private JSONObject requestObj;
    private com.example.samsung.tcc.classes.Request request;
    private String token;
    private CheckHost host = new CheckHost();

    public ListaCardResiduoActivity(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        session = new SessionManager(getContext());
        session.checkLogin();

        HashMap<String, String> usuarioLogado = session.getInfoUsuario();
        codU = Integer.parseInt(usuarioLogado.get(SessionManager.KEY_CODU));
        nomeU = usuarioLogado.get(SessionManager.KEY_NOME);
        datanascU = usuarioLogado.get(SessionManager.KEY_DATA);
        emailU = usuarioLogado.get(SessionManager.KEY_EMAIL);
        token = usuarioLogado.get(SessionManager.KEY_TOKEN);
        usuario = new Usuario(codU, nomeU, datanascU, emailU);

        setHasOptionsMenu(true);

        pDialogFav = new ProgressDialog(getActivity());
        pDialogFav.setCancelable(false);
        pDialogFav.show();

        Thread favThread = new Thread(new Runnable() {
            boolean responseError = false;
            @Override
            public void run() {
                if(host.isHostReachable(DrawerActivity.WS_URL, 8008, 5000)){
                    //while (residuoFavorito.size() <= 0 /*|| residuoFavorito.size() < 0 || !responseError*/){
                        Log.i("listaResiduosFav0", String.valueOf(residuoFavorito.size()));
                        listaResiduosFav(new VolleyCallback() {
                            @Override
                            public void onSuccess(JSONArray response) {
                                Type listType = new TypeToken<ArrayList<Residuo>>() {
                                }.getType();
                                residuoFavorito = gson.fromJson(response.toString(), listType);
                                Log.i("listaResiduosFav", String.valueOf(residuoFavorito.size()));
                                //fav = true;
                                if(layoutManager != null && recyclerView != null && adapter != null){
                                    recyclerView.setLayoutManager(layoutManager);
                                    adapter = new RecyclerViewAdapter(getActivity(), infoResiduos, residuoFavorito, usuario);
                                    recyclerView.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                } else if(getView() != null){
                                    recyclerView = (RecyclerView) getView().findViewById(R.id.recyclerView);
                                    layoutManager = new LinearLayoutManager(getActivity());
                                    recyclerView.setLayoutManager(layoutManager);
                                    adapter = new RecyclerViewAdapter(getActivity(), infoResiduos, residuoFavorito, usuario);
                                    recyclerView.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                }
                            }

                            @Override
                            public void onSuccess(JSONObject response) {
                            }

                            @Override
                            public void onError(VolleyError error) {
                                final VolleyError volleyError = error;
                                responseError = true;
                                try {
                                    if (error.networkResponse.statusCode != 0) {
                                        hideProgressDialog();
                                        Log.i("statusCode", String.valueOf(error.networkResponse.statusCode));
                                         /*getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity(),
                                                        "Ocorreu um problema na hora de listar os resíduos favoritos: " + volleyError.networkResponse.statusCode,
                                                        Toast.LENGTH_LONG)
                                                        .show();
                                            }
                                        });*/
                                    } else if (error instanceof NetworkError) {
                                        hideProgressDialog();
                                        Log.i("networkError", error.getMessage());
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity(),
                                                        "Network Error: " + volleyError.getMessage(),
                                                        Toast.LENGTH_LONG)
                                                        .show();
                                            }

                                        });
                                    } else if (error instanceof ServerError) {
                                        hideProgressDialog();
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity(),
                                                        "Server Error: " + volleyError.getMessage(),
                                                        Toast.LENGTH_LONG)
                                                        .show();
                                            }

                                        });
                                    } else if (error instanceof AuthFailureError) {
                                        Log.i("authError", error.getMessage());
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity(),
                                                        "AuthFailure Error: " + volleyError.getMessage(),
                                                        Toast.LENGTH_LONG)
                                                        .show();
                                            }

                                        });
                                    } else if (error instanceof ParseError) {
                                        hideProgressDialog();
                                        Log.i("parseError", error.getMessage());
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity(),
                                                        "Parse Error: " + volleyError.getMessage(),
                                                        Toast.LENGTH_LONG)
                                                        .show();
                                            }

                                        });
                                    } else if (error instanceof NoConnectionError) {
                                        hideProgressDialog();
                                        Log.i("noconnectionError", error.getMessage());
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity(),
                                                        "NoConnection Error: " + volleyError.getMessage(),
                                                        Toast.LENGTH_LONG)
                                                        .show();
                                            }

                                        });
                                    } else if (error instanceof TimeoutError) {
                                        hideProgressDialog();
                                        Log.i("timeoutError", error.getMessage());
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity(),
                                                        "Timeout Error: " + volleyError.getMessage(),
                                                        Toast.LENGTH_LONG)
                                                        .show();
                                            }

                                        });
                                    } else {
                                        Log.i("else", error.getMessage());
                                        hideProgressDialog();
                                        Log.i("eNF", volleyError.getMessage());
                                        /*getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getActivity(),
                                                        "Ocorreu um problema na hora de listar os resíduos favoritos: " + volleyError.getMessage(),
                                                        Toast.LENGTH_LONG)
                                                        .show();
                                            }

                                        });*/
                                    }
                                } catch (final Exception e) {
                                    Log.i("exececaoListaFav", error.getMessage() + " & " + e.getMessage());
                                    hideProgressDialog();
                                    /*getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getActivity(),
                                                    "Ocorreu uma exceção na hora de listar os resíduos favoritos: " + e.getMessage(),
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                    });*/
                                }
                            }
                        });//}
                    //}

                    if (!residuoFavorito.isEmpty()){
                        Log.i("favMsg1", String.valueOf(residuoFavorito.size()));
                        favHandler.sendEmptyMessage(1);
                    } else {
                        Log.i("favMsg0", String.valueOf(residuoFavorito.size()));
                        favHandler.sendEmptyMessage(0);
                    }
                    //}
                } else {
                    hideProgressDialog();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(),
                                    "Não foi possíverl conectar ao servidor. Por favor, tente mais tarde.",
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }
                if(pDialogFav.isShowing()){
                    pDialogFav.dismiss();
                }
            }
        });

        favThread.start();

        for(int i = 0; i < 3; i++) {
            if(residuoFavorito.isEmpty()){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Log.i("sleep", String.valueOf(i) + " & " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }

        new GetResiduos().execute();
    }

    private Handler favHandler = new Handler(){
        @Override
        public void handleMessage(Message msg){
            if(msg.what != 1){
                hideProgressDialog();
                Log.i("favHandler0", "Não foi possível retornar os favoritos.");
            } else {
                hideProgressDialog();
                Log.i("favHandler1", "Favoritos encontrados.");
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_lista_card_residuo, container, false);

        //setHasOptionsMenu(true);
        return view;
    }

    private class GetResiduos extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Por favor, aguarde...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected Void doInBackground(Void... arg0) {
            Log.i("fav1", String.valueOf(residuoFavorito.size()));
            if(host.isHostReachable(DrawerActivity.WS_URL, 8008, 5000)){//127.0.1.1

                HttpHandler sh = new HttpHandler();

                PathClass path = new PathClass(getContext());
                String url = "";
                String jsonStr = "";
                try {
                    url = path.getServerPath() + "/residuo/lista";
                    jsonStr = sh.makeServiceCall(url, "GET", null, getContext());
                } catch (IOException e) {
                    Log.e("url", url);
                    e.printStackTrace();
                }
                if(jsonStr != null){
                    try{
                        Type listType = new TypeToken<ArrayList<Residuo>>(){}.getType();
                        infoResiduos = (ArrayList<Residuo>) gson.fromJson(jsonStr, listType);
                    }catch (final Exception e){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(),
                                        getString(R.string.json_erro_parse) + ": " + e.getMessage(),
                                        Toast.LENGTH_LONG)
                                        .show();
                            }
                        });
                    }
                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(),
                                    getString(R.string.json_erro_servidor),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }

                    });
                }
            } else {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "Não foi possíverl conectar ao servidor. Por favor, tente mais tarde.", Toast.LENGTH_LONG).show();
                    }

                });
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            hideProgressDialog();

            recyclerView = (RecyclerView) getView().findViewById(R.id.recyclerView);
            layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);
            Log.i("fav2", String.valueOf(residuoFavorito.size()));
            adapter = new RecyclerViewAdapter(getActivity(), infoResiduos, residuoFavorito, usuario);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater){
        menu.clear();
        super.onCreateOptionsMenu(menu, menuInflater);
        menuInflater.inflate(R.menu.drawer, menu);
        MenuItem item = menu.findItem(R.id.buscaToolbar);
        final SearchView sv = (SearchView) item.getActionView();
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String txt) {
                adapter.filtraBusca(txt);
                return true;
            }
        });

        MenuItem goToMapa = menu.findItem(R.id.mapaToolbar);
        goToMapa.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                ((DrawerActivity) getActivity()).selectedResiduos = adapter.getResiduosSelecionados();

                Fragment fragment = new MapaFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frameContent, fragment);
                fragmentTransaction.commitAllowingStateLoss();

                return true;
            }
        });
    }

    private void listaResiduosFav(final VolleyCallback callback){
        path = new PathClass(getContext());
        if(checkInternetConnection()){
            try {
                session = new SessionManager(getContext());
                session.checkLogin();
                url = path.getServerPath() + "/usuario/favoritos";
                request = new com.example.samsung.tcc.classes.Request(gson.toJson(usuario), emailU, token);
                requestObj = new JSONObject(gson.toJson(request));

                CustomJsonArrayRequest residuoFavReq = new CustomJsonArrayRequest(Request.Method.POST , url, requestObj, getContext(),
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                callback.onSuccess(response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                callback.onError(error);
                            }
                        }


                        );
                RequestQueue requestQueue = Volley.newRequestQueue(getContext());
                requestQueue.add(residuoFavReq);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    private boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        // test for connection
        if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isConnectingToInternet() {
        ConnectivityManager connectivity = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (info != null && info.isConnected()){
                try {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection) url
                            .openConnection();
                    urlc.setConnectTimeout(3000);
                    urlc.connect();
                    if (urlc.getResponseCode() == 200) {
                        return true;
                    }
                } catch (MalformedURLException e1) {
                    e1.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public void refreshItem(){
        adapter.notifyDataSetChanged();
    }

    public void hideProgressDialog(){
        if(pDialog.isShowing()){
            pDialog.dismiss();
        }
        if(pDialogFav.isShowing()){
            pDialogFav.dismiss();
        }
    }
}
