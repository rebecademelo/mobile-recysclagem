package com.example.samsung.tcc.classes;

/**
 * Created by rodtw on 04/06/2017.
 */

public class UsuarioResiduo {
    private Usuario usuario;
    private Residuo residuo;

    public UsuarioResiduo(Usuario usuario, Residuo residuo){
        this.setUsuario(usuario);
        this.setResiduo(residuo);
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Residuo getResiduo() {
        return residuo;
    }

    public void setResiduo(Residuo residuo) {
        this.residuo = residuo;
    }

}
