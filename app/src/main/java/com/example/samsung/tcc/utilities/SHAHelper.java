package com.example.samsung.tcc.utilities;

import java.security.MessageDigest;

/**
 * Created by rodtw on 25/06/2017.
 */

public class SHAHelper {

    private MessageDigest md;

    public SHAHelper() {
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getSHA256(String toConvert) {
        MessageDigest myMd = md;
        myMd.update( (toConvert).getBytes() );
        return bytesToHex(myMd.digest());
    }

    private String bytesToHex(byte[] bytes) {
        StringBuffer result = new StringBuffer();
        for (byte b : bytes) result.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
        return result.toString();
    }

}
