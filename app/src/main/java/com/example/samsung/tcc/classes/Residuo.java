package com.example.samsung.tcc.classes;

import android.content.Context;

import com.example.samsung.tcc.utilities.PropertySource;

import java.io.IOException;
import java.io.Serializable;

public class Residuo implements Serializable {
	private int codR;
	private String nome;
	private String descricao;
	private String imagemResiduo;
	private Categoria categoria;
	private boolean isSelected;
	//private List<Dica> dicas;
	
	public Residuo(int codR, String nome, String descricao, String imagemResiduo, Categoria categoria, boolean isSelected) {
		super();
		this.codR = codR;
		this.nome = nome;
		this.descricao = descricao;
		this.setImagemResiduo(imagemResiduo);
		this.categoria = categoria;
		this.isSelected = isSelected;
	}

	public Residuo(){
	}

	public int getCodR() {
		return codR;
	}

	public void setCodR(int codR) {
		this.codR = codR;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getImagemResiduo() {
		return imagemResiduo;
	}

	public void setImagemResiduo(String imagemResiduo) {
		this.imagemResiduo = imagemResiduo;
	}

	public void fixImagemLocalhost (Context context){
		if(imagemResiduo.toLowerCase().contains("localhost")){
			try{
				this.imagemResiduo = imagemResiduo.replace("localhost",
						PropertySource.getProperty("resysclagem.service.host", context)
				);
			} catch (IOException ioe){
				ioe.printStackTrace();
			}
		}
	}


	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean selected) {
		isSelected = selected;
	}/**/

	@Override
	public boolean equals(Object obj){
		Residuo res = (Residuo) obj;
		return this.codR == res.codR;
	}
}
