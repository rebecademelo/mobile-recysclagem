package com.example.samsung.tcc.utilities;

import android.content.Context;
import android.util.Base64;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rebeca de Melo on 15/03/2017.
 */
public class CustomJSONObjectRequest extends JsonObjectRequest {

    private String baUsuario;
    private String baSenha;

    public CustomJSONObjectRequest(String url, JSONObject jsonRequest, Context context,
                                   Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, jsonRequest, listener, errorListener);
        try {
            baUsuario = PropertySource.getProperty("resysclagem.service.ba.usuario", context);
            baSenha = PropertySource.getProperty("resysclagem.service.ba.senha", context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public CustomJSONObjectRequest(int method, String url, JSONObject jsonRequest, Context context,
                                   Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
        try {
            baUsuario = PropertySource.getProperty("resysclagem.service.ba.usuario", context);
            baSenha = PropertySource.getProperty("resysclagem.service.ba.senha", context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<String, String>();
        //headers.put("Content-Type", "application/json; charset=utf-8");
        String creds = String.format("%s:%s",baUsuario,baSenha);
        String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
        headers.put("Authorization", auth);
        return headers;
    }
    @Override
    public RetryPolicy getRetryPolicy(){
        // here you can write a custom retry policy
        return super.getRetryPolicy();
    }
}
