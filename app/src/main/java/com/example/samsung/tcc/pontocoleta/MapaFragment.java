package com.example.samsung.tcc.pontocoleta;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
//import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
//import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.samsung.tcc.descarte.MeusDescartesFragment;
import com.example.samsung.tcc.utilities.CheckHost;
import com.example.samsung.tcc.utilities.CustomJSONObjectRequest;
import com.example.samsung.tcc.utilities.CustomJsonArrayRequest;
import com.example.samsung.tcc.R;
import com.example.samsung.tcc.classes.Descarte;
import com.example.samsung.tcc.classes.MsgResponse;
import com.example.samsung.tcc.classes.PontoColeta;
import com.example.samsung.tcc.classes.Residuo;
import com.example.samsung.tcc.classes.SessionManager;
import com.example.samsung.tcc.classes.Usuario;
import com.example.samsung.tcc.classes.VolleyCallback;
import com.example.samsung.tcc.drawer.DrawerActivity;
import com.example.samsung.tcc.utilities.PathClass;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class MapaFragment extends Fragment implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private MapView mapView;
    private GoogleMap mapa;
    private GoogleApiClient googleApiClient;
    private Location lastLocation;
    private Marker currentLocationMarker;
    private LocationRequest locationRequest;
    public static final int PERMISSIONS_REQUEST_LOCATION = 99;
    private Gson gson = new Gson();
    private ProgressDialog pDialog;
    private AlertDialog alertDialog;
    private ArrayList<PontoColeta> listaPontos;
    private ArrayList<Residuo> residuosSelecionados = new ArrayList<>();
    private ArrayList<Residuo> copiaResiduosSelecionados;
    private PontoColeta p;
    private Descarte descarte;
    private Usuario usuario;
    private TextView nome;
    private TextView endereco;
    private TextView horaAtendimento;
    private TextView telefone;
    private TextView email;
    private TextView residuos;
    private SessionManager session;
    private int codU;
    private String nomeU;
    private String datanascU;
    private String emailU;
    private String token;
    private String message;
    private Handler handler;
    private MarkerOptions markerOptions;
    private Marker marker;
    private com.example.samsung.tcc.classes.Request request;
    private PathClass path;
    private String url = "";
    private JSONObject requestObj;
    private HashMap<Marker, PontoColeta> pinInfo;
    private CheckHost host = new CheckHost();
    private boolean status = false;

    public MapaFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        session = new SessionManager(getContext());
        session.checkLogin();

        HashMap<String, String> usuarioLogado = session.getInfoUsuario();
        codU = Integer.parseInt(usuarioLogado.get(SessionManager.KEY_CODU));
        nomeU = usuarioLogado.get(SessionManager.KEY_NOME);
        datanascU = usuarioLogado.get(SessionManager.KEY_DATA);
        emailU = usuarioLogado.get(SessionManager.KEY_EMAIL);
        token = usuarioLogado.get(SessionManager.KEY_TOKEN);

        listaPontos = new ArrayList<>();

        carregaPontos(new VolleyCallback() {
            @Override
            public void onSuccess(JSONArray response) {
                Type listType = new TypeToken<ArrayList<PontoColeta>>() {
                }.getType();
                listaPontos = (ArrayList<PontoColeta>) gson.fromJson(response.toString(), listType);
                Log.i("listaPontos", String.valueOf(listaPontos.size()));
                hideProgressDialog();
            }

            @Override
            public void onSuccess(JSONObject response) {

            }

            @Override
            public void onError(VolleyError error) {
                if(error != null){
                //Log.i("volleyError", error.getMessage());
                final VolleyError volleyError = error;
                hideProgressDialog();
                if (error.networkResponse != null && error.networkResponse.statusCode != 0) {
                    Log.i("JSONExceptionMapa", error.getMessage() + " " +  error + " " + error.getStackTrace().toString() + " " + error.getCause());
                    hideProgressDialog();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(),
                                    getString(R.string.json_erro_parse) + volleyError.networkResponse.statusCode,
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                } else {
                    Log.i("JSONExceptionMapa", error.getMessage() + " " +  error + " " + error.getStackTrace().toString() + " " + error.getCause());
                    hideProgressDialog();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(),
                                    getString(R.string.json_erro_parse) + volleyError.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }
            }}
        });

        for(int i = 0; i < 5; i++){
            if(listaPontos.isEmpty()) {
                try {
                    Log.i("sleep", "Thread sleep: " + i);
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Log.i("interruptedException", e.getMessage());
                }
            } else {
                break;
            }
        }

        /*Thread pontoThread = new Thread(new Runnable() {
            boolean responseError = false;
            @Override
            public void run() {
                if(host.isHostReachable("10.0.3.2", 8008, 5000)){
                    carregaPontos(new VolleyCallback() {
                        @Override
                        public void onSuccess(JSONArray response) {
                            Type listType = new TypeToken<ArrayList<PontoColeta>>() {
                            }.getType();
                            listaPontos = (ArrayList<PontoColeta>) gson.fromJson(response.toString(), listType);
                            Log.i("listaPontos", String.valueOf(listaPontos.size()));
                            //hideProgressDialog();
                        }

                        @Override
                        public void onSuccess(JSONObject response) {

                        }

                        @Override
                        public void onError(VolleyError error) {
                            Log.i("volleyError", error.getMessage());
                            final VolleyError volleyError = error;
                            hideProgressDialog();
                            responseError = true;
                            if (error.networkResponse != null && error.networkResponse.statusCode != 0) {
                                Log.i("JSONExceptionMapa", error.getMessage() + " " +  error + " " + error.getStackTrace().toString() + " " + error.getCause());
                                hideProgressDialog();
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getActivity(),
                                                "Erro ao fazer parse no json: " + volleyError.networkResponse.statusCode,
                                                Toast.LENGTH_LONG)
                                                .show();
                                    }
                                });
                            } else {
                                Log.i("JSONExceptionMapa", error.getMessage() + " " +  error + " " + error.getStackTrace().toString() + " " + error.getCause());
                                hideProgressDialog();
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getActivity(),
                                                R.string.json_erro_parse + volleyError.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    }
                                });
                            }
                        }
                    });

                    for(int i = 0; i < 5; i++){
                        if(listaPontos.isEmpty()) {
                            try {
                                Log.i("sleep", "Thread sleep: " + i);
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                                Log.i("interruptedException", e.getMessage());
                            }
                        } else {
                            break;
                        }
                    }

                    if (!listaPontos.isEmpty()){
                        Log.i("pontoMsg1", String.valueOf(listaPontos.size()));
                        pontoHandler.sendEmptyMessage(1);
                    } else {
                        Log.i("pontoMsg0", String.valueOf(listaPontos.size()));
                        pontoHandler.sendEmptyMessage(0);
                    }
                } else {
                    hideProgressDialog();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(),
                                    "Não foi possíverl conectar ao servidor. Por favor, tente mais tarde.",
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }
            }
        });

        pontoThread.start();*/

        /**/

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
    }

    private Handler pontoHandler = new Handler(){
        @Override
        public void handleMessage(Message msg){
            if(msg.what != 1){
                hideProgressDialog();
                Log.i("pontoHandler0", "Não foi possível retornar os pontos de coleta.");
            } else {
                hideProgressDialog();
                Log.i("pontoHandler1", "Pontos de coleta encontrados.");
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mapa, container, false);

        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapa = googleMap;
        //mapa.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mapa.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mapa.setMyLocationEnabled(true);
        }
        if(mapa != null){
            pinInfo = new HashMap<>();
            for(PontoColeta pc : listaPontos){
                this.p = pc;
                LatLng ponto = new LatLng(p.getLatitude(), p.getLongitude());
                markerOptions = new MarkerOptions();
                marker = googleMap.addMarker(markerOptions.title(p.getNome()).position(new LatLng(p.getLatitude(), p.getLongitude())));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ponto, 15));
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
                pinInfo.put(marker, pc);
            }

            googleMap.setInfoWindowAdapter(new PontoInfoWindow(getContext(), pinInfo));
            googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    if(copiaResiduosSelecionados != null && copiaResiduosSelecionados.size() > 0){
                        Log.i("copiaNotNull", String.valueOf(copiaResiduosSelecionados.size()));
                        PontoColeta ponto = pinInfo.get(marker);

                        Location p = new Location("ponto");
                        p.setLatitude(ponto.getLatitude());
                        p.setLongitude(ponto.getLongitude());
                        if(currentLocationMarker != null){
                            LatLng current = currentLocationMarker.getPosition();

                            Location c = new Location("currentlocation");
                            c.setLatitude(current.latitude);
                            c.setLongitude(current.longitude);

                            float distancia = p.distanceTo(c);
                            Log.i("distancia", String.valueOf(distancia));

                            //if(currentLocationMarker != null && currentLocationMarker.equals(marker.getPosition())){
                            if(distancia <= 200.0){
                                // chama o metodo q faz o descarte
                                Log.i("pontoDescarte", ponto.getNome());
                                status = true;
                                registraDescarte(new VolleyCallback() {
                                    @Override
                                    public void onSuccess(JSONArray response) {

                                    }

                                    @Override
                                    public void onSuccess(JSONObject response) {
                                        MsgResponse msgResponse = gson.fromJson(response.toString(), MsgResponse.class);
                                        message = msgResponse.getMessage();
                                    }

                                    @Override
                                    public void onError(VolleyError error) {
                                        final VolleyError volleyError = error;
                                        try {
                                            if (error.networkResponse.statusCode != 0) {
                                                hideProgressDialog();
                                                Log.i("statusCode", String.valueOf(error.networkResponse.statusCode));
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Toast.makeText(getActivity(),
                                                                "Ocorreu um problema: " + volleyError.networkResponse.statusCode,
                                                                Toast.LENGTH_LONG)
                                                                .show();
                                                    }
                                                });
                                            } else if (error instanceof NetworkError) {
                                                hideProgressDialog();
                                                Log.i("networkError", error.getMessage());
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Toast.makeText(getActivity(),
                                                                "Network Error: " + volleyError.getMessage(),
                                                                Toast.LENGTH_LONG)
                                                                .show();
                                                    }

                                                });
                                            } else if (error instanceof ServerError) {
                                                hideProgressDialog();
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Toast.makeText(getActivity(),
                                                                "Server Error: " + volleyError.getMessage(),
                                                                Toast.LENGTH_LONG)
                                                                .show();
                                                    }

                                                });
                                            } else if (error instanceof AuthFailureError) {
                                                Log.i("authError", error.getMessage());
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Toast.makeText(getActivity(),
                                                                "AuthFailure Error: " + volleyError.getMessage(),
                                                                Toast.LENGTH_LONG)
                                                                .show();
                                                    }

                                                });
                                            } else if (error instanceof ParseError) {
                                                hideProgressDialog();
                                                Log.i("parseError", error.getMessage());
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Toast.makeText(getActivity(),
                                                                "Parse Error: " + volleyError.getMessage(),
                                                                Toast.LENGTH_LONG)
                                                                .show();
                                                    }

                                                });
                                            } else if (error instanceof NoConnectionError) {
                                                hideProgressDialog();
                                                Log.i("noconnectionError", error.getMessage());
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Toast.makeText(getActivity(),
                                                                "NoConnection Error: " + volleyError.getMessage(),
                                                                Toast.LENGTH_LONG)
                                                                .show();
                                                    }

                                                });
                                            } else if (error instanceof TimeoutError) {
                                                hideProgressDialog();
                                                Log.i("timeoutError", error.getMessage());
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Toast.makeText(getActivity(),
                                                                "Timeout Error: " + volleyError.getMessage(),
                                                                Toast.LENGTH_LONG)
                                                                .show();
                                                    }

                                                });
                                            } else {
                                                Log.i("else", error.getMessage());
                                                hideProgressDialog();
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Toast.makeText(getActivity(),
                                                                "Ocorreu um problema na hora de listar os resíduos favoritos: " + volleyError.getMessage(),
                                                                Toast.LENGTH_LONG)
                                                                .show();
                                                    }

                                                });
                                                /*Toast.makeText(getContext(),
                                                        error.getMessage(),
                                                        Toast.LENGTH_LONG)
                                                        .show();*/
                                            }
                                        } catch (final Exception e) {
                                            Log.i("exececaoListaFav", error.getMessage() + " & " + e.getMessage());
                                            hideProgressDialog();
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(getActivity(),
                                                            "Ocorreu uma exceção: " + e.getMessage(),
                                                            Toast.LENGTH_LONG)
                                                            .show();
                                                }

                                            });
                                        }
                                    }
                                }, status);
                                alertDialog = new AlertDialog.Builder(getActivity()).create();
                                alertDialog.setTitle(getString(R.string.descarte_sucesso)); //R.string.descarte
                                alertDialog.setMessage(getString(R.string.descarte_sucesso));
                                alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Toast.makeText(getActivity().getApplicationContext(), getString(R.string.descarte_concluído), Toast.LENGTH_LONG).show();
                                    }
                                });
                                alertDialog.show();
                            } else {
                                Log.i("pontoDescarte", ponto.getNome());
                                Thread descartePendThread = new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(host.isHostReachable("10.0.3.2", 8008, 5000)) {
                                            registraDescarte(new VolleyCallback() {
                                                @Override
                                                public void onSuccess(JSONArray response) {

                                                }

                                                @Override
                                                public void onSuccess(JSONObject response) {
                                                    MsgResponse msgResponse = gson.fromJson(response.toString(), MsgResponse.class);
                                                    message = msgResponse.getMessage();
                                                }

                                                @Override
                                                public void onError(VolleyError error) {
                                                    final VolleyError volleyError = error;
                                                    try {
                                                        if (error.networkResponse.statusCode != 0) {
                                                            hideProgressDialog();
                                                            Log.i("statusCode", String.valueOf(error.networkResponse.statusCode));
                                                            getActivity().runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    Toast.makeText(getActivity(),
                                                                            "Ocorreu um problema: " + volleyError.networkResponse.statusCode,
                                                                            Toast.LENGTH_LONG)
                                                                            .show();
                                                                }
                                                            });
                                                        } else if (error instanceof NetworkError) {
                                                            hideProgressDialog();
                                                            Log.i("networkError", error.getMessage());
                                                            getActivity().runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    Toast.makeText(getActivity(),
                                                                            "Network Error: " + volleyError.getMessage(),
                                                                            Toast.LENGTH_LONG)
                                                                            .show();
                                                                }

                                                            });
                                                        } else if (error instanceof ServerError) {
                                                            hideProgressDialog();
                                                            getActivity().runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    Toast.makeText(getActivity(),
                                                                            "Server Error: " + volleyError.getMessage(),
                                                                            Toast.LENGTH_LONG)
                                                                            .show();
                                                                }

                                                            });
                                                        } else if (error instanceof AuthFailureError) {
                                                            Log.i("authError", error.getMessage());
                                                            getActivity().runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    Toast.makeText(getActivity(),
                                                                            "AuthFailure Error: " + volleyError.getMessage(),
                                                                            Toast.LENGTH_LONG)
                                                                            .show();
                                                                }

                                                            });
                                                        } else if (error instanceof ParseError) {
                                                            hideProgressDialog();
                                                            Log.i("parseError", error.getMessage());
                                                            getActivity().runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    Toast.makeText(getActivity(),
                                                                            "Parse Error: " + volleyError.getMessage(),
                                                                            Toast.LENGTH_LONG)
                                                                            .show();
                                                                }

                                                            });
                                                        } else if (error instanceof NoConnectionError) {
                                                            hideProgressDialog();
                                                            Log.i("noconnectionError", error.getMessage());
                                                            getActivity().runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    Toast.makeText(getActivity(),
                                                                            "NoConnection Error: " + volleyError.getMessage(),
                                                                            Toast.LENGTH_LONG)
                                                                            .show();
                                                                }

                                                            });
                                                        } else if (error instanceof TimeoutError) {
                                                            hideProgressDialog();
                                                            Log.i("timeoutError", error.getMessage());
                                                            getActivity().runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    Toast.makeText(getActivity(),
                                                                            "Timeout Error: " + volleyError.getMessage(),
                                                                            Toast.LENGTH_LONG)
                                                                            .show();
                                                                }

                                                            });
                                                        } else {
                                                            Log.i("else", error.getMessage());
                                                            hideProgressDialog();
                                                            getActivity().runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    Toast.makeText(getActivity(),
                                                                            "Ocorreu um problema: " + volleyError.getMessage(),
                                                                            Toast.LENGTH_LONG)
                                                                            .show();
                                                                }

                                                            });
                                                        }
                                                    } catch (final Exception e) {
                                                        Log.i("exececao", error.getMessage() + " & " + e.getMessage());
                                                        hideProgressDialog();
                                                        getActivity().runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                Toast.makeText(getActivity(),
                                                                        getString(R.string.exception_mensagem) + e.getMessage(),
                                                                        Toast.LENGTH_LONG)
                                                                        .show();
                                                            }

                                                        });
                                                    }
                                                }
                                            }, status);
                                        } else {
                                            hideProgressDialog();
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(getActivity(),
                                                            getString(R.string.servidor_erro),
                                                            Toast.LENGTH_LONG)
                                                            .show();
                                                }
                                            });
                                        }
                                    }
                                });

                                descartePendThread.start();
                                /*try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }*/
                                alertDialog = new AlertDialog.Builder(getActivity()).create();
                                alertDialog.setTitle(getString(R.string.descarte_pendente)); //R.string.descarte
                                alertDialog.setMessage(getString(R.string.descarte_salvo_aviso)); //getString(R.string.descarte_sucesso)
                                alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Toast.makeText(getActivity().getApplicationContext(),
                                                getString(R.string.descarte_pendente),
                                                Toast.LENGTH_LONG)
                                                .show();
                                        Fragment fragment = new MeusDescartesFragment();
                                        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                                        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                                        fragmentTransaction.replace(R.id.frameContent, fragment);
                                        fragmentTransaction.commitAllowingStateLoss();
                                    }
                                });
                                alertDialog.show();
                            }
                        } else {
                            Toast.makeText(getContext(),
                                    "É necessário estar com o gps ligado para realizar esta ação.",
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    } else {
                        Toast.makeText(getContext(),
                                getString(R.string.descarte_sem_residuo),
                                Toast.LENGTH_LONG)
                                .show();
                    }
                }
            });
        }else{
            Toast.makeText(getContext(),
                    getString(R.string.carregar_mapa_erro) ,
                    Toast.LENGTH_LONG)
                    .show();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
        if(googleApiClient.isConnected()){
            googleApiClient.disconnect();
        }
    }

    /*@Override
    public void onDestroyView(){
        super.onDestroyView();
        MapaFragment mapaFragment = (MapaFragment) getFragmentManager().findFragmentById(R.id.mapView);
        if(mapaFragment != null){
            getFragmentManager().beginTransaction().remove(mapaFragment).commit();
        }
        Fragment fragment = null;//getParentFragment();
        if (fragment != null) {
            fragment =  new MapaFragment();
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.remove(fragment);
            ft.commit();
        }
    }*/

    @Override
    public void onStop(){
        super.onStop();
        if(googleApiClient.isConnected()){
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mapView != null){
            mapView.onResume();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mapView!=null){
            mapView.onDestroy();
        }
        if(googleApiClient != null){
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if(mapView!=null){
            mapView.onLowMemory();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        lastLocation = location;
        if (currentLocationMarker != null) {
            currentLocationMarker.remove();
        }

        // coloca o marker na localização atual
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Posição atual");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        currentLocationMarker = mapa.addMarker(markerOptions);

        //move map camera
        mapa.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mapa.animateCamera(CameraUpdateFactory.zoomTo(11));

        //stop location updates
        if (googleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST_LOCATION);
            } else {
                // faz request da permissao
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_LOCATION: {
                // se o request é cancelado, o array de resultados fica vazio
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(getContext(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (googleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mapa.setMyLocationEnabled(true);
                    }
                } else {
                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(getContext(), getString(R.string.permissao_negada), Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    private void carregaPontos(final VolleyCallback callback){
        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.show();

        path = new PathClass(getContext());
        try {
            residuosSelecionados = ((DrawerActivity) getActivity()).selectedResiduos;
            copiaResiduosSelecionados = residuosSelecionados;
            /*if(copiaResiduosSelecionados.size() != 0){
                Log.i("copiaResiduos", String.valueOf(copiaResiduosSelecionados.size()));
            }*/

            request = new com.example.samsung.tcc.classes.Request(gson.toJson(residuosSelecionados), emailU, token);
            requestObj = new JSONObject(gson.toJson(request));

            if(residuosSelecionados == null || residuosSelecionados.isEmpty()){
                url = path.getServerPath() + "/pontocoleta/lista";
                JsonArrayRequest pontosReq = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.i("carregaPontosCallback", response.toString());
                        callback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                });
                RequestQueue requestQueue = Volley.newRequestQueue(getContext());
                requestQueue.add(pontosReq);
            } else {
                url = path.getServerPath() + "/pontocoleta/comresiduo";
                CustomJsonArrayRequest pontosSelecReq = new CustomJsonArrayRequest(Request.Method.POST, url, requestObj, getContext(),
                        new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.i("carregaPontosCallback2", response.toString());
                        //pDialog.dismiss();
                        callback.onSuccess(response);
                    }
/*
                    public void onResponse(JSONObject response) {
                        //callback.onSuccess(response);
                        try {
                            MsgResponse msg = (MsgResponse) gson.fromJson(response.toString(), MsgResponse.class);
                            pDialog.hide();
                            Toast.makeText(getContext(), msg.getMessage(), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Toast.makeText(getContext(), response.toString(), Toast.LENGTH_LONG).show();
                        }
                    }*/
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onError(error);
                    }
                });
                RequestQueue requestQueue = Volley.newRequestQueue(getContext());
                requestQueue.add(pontosSelecReq);
            }
            ((DrawerActivity) getActivity()).selectedResiduos = new ArrayList<Residuo>();
        } catch (JSONException e) {
            pDialog.hide();
            e.printStackTrace();
            Log.i("JSONExceptionMapa", e.getMessage() + " " +  e + " " + e.getStackTrace().toString() + " " + e.getCause());
            Toast.makeText(getActivity(),
                    getString(R.string.json_exception) + ": " + e.getMessage(),
                    Toast.LENGTH_LONG)
                    .show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void registraDescarte(final VolleyCallback callback, boolean status){
        path = new PathClass(getContext());
        try {
            url = path.getServerPath() + "/descarte";
            descarte = new Descarte();
            usuario = new Usuario(codU, nomeU, datanascU, emailU);
            descarte.setUsuario(usuario);
            p.getCodPC();
            p.getNome();
            p.getImagem();
            p.getEndereco();
            p.getDescricao();
            p.getTelefone();
            p.getEmail();
            p.getHoraAtendimento();
            descarte.setPontoColeta(p);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String dataDescarte = dateFormat.format(Calendar.getInstance().getTime());
            descarte.setDataD(dataDescarte);
            descarte.setStatusD(status);
            descarte.setResiduos(residuosSelecionados);

            request = new com.example.samsung.tcc.classes.Request(gson.toJson(descarte), emailU, token);
            requestObj = new JSONObject(gson.toJson(request));

            CustomJSONObjectRequest descarteReq = new CustomJSONObjectRequest(url, requestObj, getContext(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    callback.onSuccess(response);
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    callback.onError(error);
                }
            });
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(descarteReq);
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getActivity(),
                    getString(R.string.json_exception) + ": " + e.getMessage(),
                    Toast.LENGTH_LONG)
                    .show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void hideProgressDialog(){
        if(pDialog.isShowing()){
            pDialog.dismiss();
        }
    }
}
